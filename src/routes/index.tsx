import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import RouteDrawer from './drawer';

export type RootProp = {
  defaultStack: undefined;
};

export const RootStack = createStackNavigator<RootProp>();

const Routes: React.FC = () => {
  return (
    <NavigationContainer>
      <RootStack.Navigator initialRouteName="defaultStack">
        <RootStack.Screen
          name="defaultStack"
          component={RouteDrawer}
          options={{headerShown: false}}
        />
      </RootStack.Navigator>
    </NavigationContainer>
  );
};

export default Routes;
