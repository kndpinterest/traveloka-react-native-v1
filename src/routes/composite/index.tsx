import {BottomTabNavigationProp} from '@react-navigation/bottom-tabs';
import {DrawerNavigationProp} from '@react-navigation/drawer';
import {CompositeNavigationProp, RouteProp} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';
import React from 'react';
import {RootProp} from '..';
import {RootDrawerProp} from '../drawer';
import {TabRootProp} from '../tab';

type Composite = CompositeNavigationProp<
  BottomTabNavigationProp<TabRootProp, 'defaultTab'>,
  CompositeNavigationProp<
    DrawerNavigationProp<RootDrawerProp>,
    StackNavigationProp<RootProp>
  >
>;

type DrawerRouteCredentials = RouteProp<RootDrawerProp, "credentials">

export interface NavigationProp {
  route: DrawerRouteCredentials
  navigation: Composite;
}
