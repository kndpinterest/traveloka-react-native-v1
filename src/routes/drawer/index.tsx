import {
  createDrawerNavigator,
  DrawerContentOptions,
} from '@react-navigation/drawer';
import {DrawerActions} from '@react-navigation/native';
import React from 'react';
import {
  FlatList,
  GestureResponderEvent,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import BarSvg from '../../assets/svg/bar_traveloka.svg';
import Credentials from '../../screen/auth/credentials';
import {NavigationProp} from '../composite';
import TraveloSvg from '../../assets/svg/traveloka.svg';
import TraveloSvg_ from '../../assets/svg/travelokaSvg.svg';
import TitleTraveloSvg from '../../assets/svg/title_traveloka.svg';
import UserSVg from '../../assets/svg/user.svg';
import Exit from '../../assets/svg/left-arrow.svg';
import {MenuArray} from '../../prefix/drawer';
import Svg from 'react-native-svg';
import RouteTab from '../tab';
import {CredentialsScreenState} from '../../screen/auth/credentials/context';
import PlainComponent from '../../screen/plain';

export type DrawerRouteName =
  | 'defaultDrawer'
  | 'credentials'
  | 'defaultStack'
  | 'defaultTab'
  | 'plain';

export type RootDrawerProp = {
  defaultDrawer: undefined;
  credentials: {
    name: CredentialsScreenState;
  };
  plain: undefined;
};

export const RootDrawer = createDrawerNavigator<RootDrawerProp>();

// Drawer List Custom

export interface DrawerListProp {
  name: string;
  icon: React.ReactElement<Svg>;
  router: DrawerRouteName;
  params: CredentialsScreenState;
}

interface FlatLitsProp {
  index: number;
  item: DrawerListProp;
}

const CustomDrawer = ({navigation}: DrawerContentOptions & NavigationProp) => {
  const onRouter = (args: DrawerRouteName, params: CredentialsScreenState) => {
    switch (args) {
      case 'credentials':
        navigation.navigate(args, {
          name: params,
        });
        break;
      case 'defaultStack':
        navigation.push(args);
        break;
      default:
        break;
    }
  };
  function renderItem({index, item}: FlatLitsProp) {
    return (
      <View
        key={index}
        style={{
          marginBottom: 15,
          borderWidth: 0.9,
          borderTopColor: 'transparent',
          borderRightColor: 'transparent',
          borderLeftColor: 'transparent',
          borderBottomColor:
            item.name === 'Unduh Aplikasi' || item.name === 'Asuransi'
              ? 'black'
              : 'transparent',
          paddingBottom: 10,
        }}>
        <TouchableOpacity
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            width: '90%',
            alignSelf: 'center',
          }}
          onPress={onRouter.bind(item, item.router, item.params)}>
          {item.icon}
          <Text
            style={{
              color: 'rgb(104, 113, 118)',
              fontWeight: 'bold',
              marginLeft: 10,
              fontSize: 18,
            }}>
            {item.name}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }

  const isCloseDrawer = (args: GestureResponderEvent) => {
    args.preventDefault();
    navigation.dispatch(DrawerActions.closeDrawer());
  };

  return (
    <View>
      <View
        style={{
          height: 60,
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
          width: '90%',
          alignSelf: 'center',
        }}>
        <TouchableOpacity
          style={{
            width: 40,
            height: 40,
            backgroundColor: 'white',
            elevation: 4,
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius: 10,
            zIndex: 10,
          }}
          onPress={isCloseDrawer}>
          <Exit width={22} height={22} stroke="black" strokeWidth={25} />
        </TouchableOpacity>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <TraveloSvg_ width={30} height={30} />
          <Text
            style={{
              fontWeight: 'bold',
              fontSize: 18,
            }}>
            Traveloka
          </Text>
        </View>
      </View>
      <FlatList
        contentContainerStyle={{
          paddingTop: 30,
          paddingBottom: 30,
        }}
        data={MenuArray}
        renderItem={renderItem}
        keyExtractor={(item) => item.name}
      />
    </View>
  );
};

// Options

const Options = (props: NavigationProp) => ({
  headerShown: true,
  headerTitleAlign: 'center',
  headerStyle: {
    backgroundColor: 'rgba(27,160,225,1.00)',
  },
  headerLeft: () => {
    const isOpenDrawer = (args: GestureResponderEvent) => {
      props.navigation.dispatch(DrawerActions.openDrawer());
    };
    return (
      <TouchableOpacity
        style={{
          marginLeft: 15,
        }}
        onPress={isOpenDrawer}>
        <BarSvg width={27} height={27} fill="black" />
      </TouchableOpacity>
    );
  },
  headerRight: () => {
    const onRouter = (params: CredentialsScreenState) => {
      props.navigation.navigate('credentials', {
        name: params,
      });
    };
    return (
      <TouchableOpacity
        style={{
          marginRight: 15,
        }}
        onPress={onRouter.bind('', 'login')}>
        <UserSVg width={23} height={23} fill="white" />
      </TouchableOpacity>
    );
  },
  headerTitle: () => {
    return (
      <View style={{flexDirection: 'row', alignItems: 'center'}}>
        <TraveloSvg width={20} height={20} fill="white" />
        <TitleTraveloSvg width={100} height={20} fill="white" />
      </View>
    );
  },
});

// Routes

const RouteDrawer: React.FC<NavigationProp> = (
  props: React.PropsWithChildren<NavigationProp>,
) => {
  return (
    <RootDrawer.Navigator
      initialRouteName="defaultDrawer"
      drawerContent={(drawer) => <CustomDrawer {...drawer} {...props} />}>
      <RootDrawer.Screen
        name="defaultDrawer"
        component={RouteTab}
        options={Options(props)}
      />
      <RootDrawer.Screen
        name="credentials"
        component={Credentials}
        options={Options(props)}
      />
      <RootDrawer.Screen
        name="plain"
        component={PlainComponent}
        options={Options(props)}
      />
    </RootDrawer.Navigator>
  );
};

export default RouteDrawer;
