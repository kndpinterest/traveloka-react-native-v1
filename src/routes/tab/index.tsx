import React from 'react';
import {
  createBottomTabNavigator,
} from '@react-navigation/bottom-tabs';
import DefaultComponent from '../../screen/default';
import {FlatList, Text, View} from 'react-native';
import {NavigationProp} from '../composite';
import {CustomTabDefault} from '../../prefix/tab';
import Svg from 'react-native-svg';

export type TabRootProp = {
  defaultTab: undefined;
};

export const TabRoot = createBottomTabNavigator<TabRootProp>();

// Custom Tab
interface FlatListProp {
  index: number;
  item: {
    name: string;
    icon: React.ReactElement<Svg>;
  };
}

const CustomTab = ({}: NavigationProp) => {
  function renderItem({index, item}: FlatListProp) {
    return (
      <View
        key={index}
        style={{
          flexDirection: 'column',
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        {item.icon}
        <Text
          style={{
            fontWeight: 'bold',
            color: 'rgb(104, 113, 118)'
          }}>
          {item.name}
        </Text>
      </View>
    );
  }
  return (
    <View
      style={{
        shadowColor: '#000',
        shadowOffset: {
          width: 0,
          height: 10,
        },
        shadowOpacity: 0.51,
        shadowRadius: 13.16,
        backgroundColor: 'white',
        elevation: 20,
      }}>
      <FlatList
        contentContainerStyle={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-around',
          paddingTop: 10,
          paddingBottom: 5,
        }}
        data={CustomTabDefault}
        renderItem={renderItem}
        keyExtractor={(item) => item.name}
      />
    </View>
  );
};

const RouteTab: React.FC<NavigationProp> = (
  props: React.PropsWithChildren<NavigationProp>,
) => {
  return (
    <TabRoot.Navigator
      initialRouteName="defaultTab"
      tabBar={(tab) => <CustomTab {...props} />}>
      <TabRoot.Screen name="defaultTab" component={DefaultComponent} />
    </TabRoot.Navigator>
  );
};

export default RouteTab;
