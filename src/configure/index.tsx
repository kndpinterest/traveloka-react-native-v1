import { applyMiddleware, createStore, Store } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunkMiddleware from 'redux-thunk'
import loggerMiddleware from 'redux-logger'
import stores_ from '../store'

export interface ApplicationState {}

export default function configureStore(preloladedState?: any) {
    const middleware = [thunkMiddleware]
    const store: Store<ApplicationState> = createStore(
        stores_,
        preloladedState,
        composeWithDevTools(
            applyMiddleware(
                ...middleware,
                loggerMiddleware
            )
        )       
    )
    return store
}