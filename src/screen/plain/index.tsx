import {View} from 'native-base';
import React from 'react';
import {
  Alert,
  Dimensions,
  FlatList,
  GestureResponderEvent,
  Image,
  NativeSyntheticEvent,
  SafeAreaView,
  ScrollView,
  Text,
  TextInputChangeEventData,
  TouchableOpacity,
} from 'react-native';

import moment from 'moment';
import SyncSvg from '../../assets/svg/sync1.svg';
import ArrowRight from '../../assets/svg/right-arrow (1).svg';
import TravelokaApp from '../../assets/svg/1608263686188-1b537ae100eea0644c92829b3fef0570 copy.svg';
import {WindowsDatePickerChangeEvent} from '@react-native-community/datetimepicker';
import Animated, {
  useAnimatedStyle,
  useSharedValue,
  withSpring,
} from 'react-native-reanimated';
import PassengerComponent from './child/component/passender.component';
import FlightClassComponent from './child/component/FlightClass.component';
import PlaneFormComponent from './child/plane.form';
import TravelArray, {TravelingArray} from './child/travel.array';
import ImageChildArray, {ImageChild} from './child/image.child.array';

export interface PlaneComponentState {
  from?: string;
  to?: string;
  date_away: Date;
  return_date: Date;
  flightPassenger: number;
  passenger: number;
  passengerChild: number;
  passengerBaby: number;
  flight_class: {
    name: string;
    index: number;
  };
  round_trip: boolean;
  show: boolean;
}

const height = -Dimensions.get('screen').height;
const heightAbsolute = 460;
const addHeight = 100;

const heightContent = 400;

export const today = new Date();

const PlaneComponent = () => {
  const [state, setState] = React.useState<PlaneComponentState>({
    from: 'Yogyakarta',
    to: 'Jakarta',
    date_away: new Date(moment().format('YYYY-MM-DD')),
    return_date: new Date(),
    passenger: 1,
    flightPassenger: 1,
    flight_class: {
      name: 'Economy',
      index: 0,
    },
    round_trip: false,
    show: false,
    passengerBaby: 0,
    passengerChild: 0,
  });

  const offsetDefault = useSharedValue(heightAbsolute);
  const animatedDefault = useAnimatedStyle(() => {
    return {
      height: withSpring(offsetDefault.value),
    };
  });

  const offsetContent = useSharedValue(heightContent);
  const animatedContent = useAnimatedStyle(() => {
    return {
      height: withSpring(offsetContent.value),
    };
  });

  const offsetPassenger = useSharedValue(height);
  const animatedPassenger = useAnimatedStyle(() => {
    return {
      left: withSpring(offsetPassenger.value),
    };
  });
  const offsetFlightClass = useSharedValue(height);
  const animatedFlightClass = useAnimatedStyle(() => {
    return {
      left: withSpring(offsetFlightClass.value),
    };
  });

  const onShowPassenger = (args: GestureResponderEvent) => {
    args.preventDefault();
    if (height === offsetPassenger.value) {
      offsetPassenger.value = 0;
    } else {
      offsetPassenger.value = height;
    }
  };

  const onShowFlightClass = (args: GestureResponderEvent) => {
    args.preventDefault();
    if (height === offsetFlightClass.value) {
      offsetFlightClass.value = 0;
    } else {
      offsetFlightClass.value = height;
    }
  };

  const onShow = () => {
    setState({
      ...state,
      show: !state.show,
    });
  };

  const onChangeFrom = (
    args: NativeSyntheticEvent<TextInputChangeEventData>,
  ) => {
    setState({
      ...state,
      from: args.nativeEvent.text,
    });
  };

  const onChangeTo = (args: NativeSyntheticEvent<TextInputChangeEventData>) => {
    setState({
      ...state,
      to: args.nativeEvent.text,
    });
  };

  const handleClickRoundTrip = (args: boolean) => {
    let date: any;
    if (args) {
      date = new Date(
        state.date_away.getFullYear(),
        state.date_away.getMonth(),
        state.date_away.getDate() + 1,
      );
    }
    if (offsetDefault.value === heightAbsolute + addHeight) {
      setState({
        ...state,
        round_trip: args,
      });
      setTimeout(() => {
        offsetDefault.value = heightAbsolute;
        offsetContent.value = heightContent;
      }, 150);
    } else {
      setState({
        ...state,
        round_trip: args,
        return_date: date ? date : state.return_date,
      });
      offsetDefault.value = heightAbsolute + addHeight;
      offsetContent.value = heightContent + addHeight;
    }
  };

  const onDateChange = (
    args: WindowsDatePickerChangeEvent | any,
    date?: Date | any,
  ) => {
    setState({
      ...state,
      date_away: date,
      show: false,
    });
  };

  const addPassengerAdul = (args: GestureResponderEvent) => {
    args.preventDefault();
    if (state.passenger !== 15) {
      setState({
        ...state,
        passenger: state.passenger + 1,
      });
    }
  };

  const removePassengerAdult = () => {
    if (state.passenger !== 1) {
      setState({
        ...state,
        passenger: state.passenger - 1,
      });
    }
  };

  const addPassengerChild = (args: GestureResponderEvent) => {
    args.preventDefault();
    if (state.passengerChild !== 5) {
      setState({
        ...state,
        passengerChild: state.passengerChild + 1,
      });
    }
  };

  const removePassengerChild = (args: GestureResponderEvent) => {
    args.preventDefault();
    if (state.passengerChild !== 1) {
      setState({
        ...state,
        passengerChild: state.passengerChild - 1,
      });
    }
  };

  const addPassengerBaby = (args: GestureResponderEvent) => {
    args.preventDefault();
    if (state.passengerBaby !== 5) {
      setState({
        ...state,
        passengerBaby: state.passengerBaby + 1,
      });
    }
  };

  const removePassengerBaby = (args: GestureResponderEvent) => {
    args.preventDefault();
    if (state.passengerBaby !== 1) {
      setState({
        ...state,
        passengerBaby: state.passengerBaby - 1,
      });
    }
  };
  const passengerOnPress = (args: GestureResponderEvent) => {
    const total = state.passenger + state.passengerChild + state.passengerBaby;
    if (state.flightPassenger !== total) {
      args.preventDefault();
      setState({
        ...state,
        flightPassenger: total,
      });
    }
    offsetPassenger.value = height;
  };

  const onChoiceFlightClass = (name: string, index: number) => {
    setState({
      ...state,
      flight_class: {
        name,
        index,
      },
    });
  };

  const onPress = () => {
    Alert.alert(
      'Output',
      'Please Open Console for looking output',
      [
        {
          text: 'Cancel',
          style: 'cancel',
        },
      ],
      {
        cancelable: false,
      },
    );
    console.log(state);
  };

  return (
    <SafeAreaView
      style={{
        flexGrow: 1,
      }}>
      <ScrollView
        contentContainerStyle={{
          flexGrow: 1,
        }}>
        <View
          style={{
            height: 120,
            width: '100%',
            backgroundColor: 'rgba(27,160,225,1.00)',
            borderBottomRightRadius: 40,
            borderBottomLeftRadius: 40,
          }}></View>
        {/* Plane Form */}
        <Animated.View
          style={[
            animatedDefault,
            {
              marginRight: 20,
              marginLeft: 20,
              position: 'absolute',
              top: 30,
              width: '90%',
              backgroundColor: 'white',
              borderRadius: 15,
              elevation: 4,
              flexDirection: 'column',
              padding: 20,
            },
          ]}>
          <PlaneFormComponent
            state={state}
            onShowFlightClass={onShowFlightClass}
            onShowPassenger={onShowPassenger}
            onDateChange={onDateChange}
            onShow={onShow}
            onChangeFrom={onChangeFrom}
            onChangeTo={onChangeTo}
            handleClickRoundTrip={handleClickRoundTrip}
            onPress={onPress}
          />
        </Animated.View>
        <Animated.View
          style={[
            animatedContent,
            {
              backgroundColor: 'transparent',
              zIndex: -1,
            },
          ]}></Animated.View>
        {/* Passenger Modal Bottom */}
        <Animated.View
          style={[
            animatedPassenger,
            {
              position: 'absolute',
              height: '100%',
              backgroundColor: 'rgba(52, 52, 52, 0.8)',
              zIndex: 10,
              width: '100%',
              justifyContent: 'flex-start',
              elevation: 4,
            },
          ]}>
          <PassengerComponent
            onShowPassenger={onShowPassenger}
            addPassengerAdul={addPassengerAdul}
            removePassengerAdult={removePassengerAdult}
            addPassengerBaby={addPassengerBaby}
            removePassengerBaby={removePassengerBaby}
            addPassengerChild={addPassengerChild}
            removePassengerChild={removePassengerChild}
            passengerOnPress={passengerOnPress}
            {...state}
          />
        </Animated.View>
        {/* Plane Modal Bottom */}
        <Animated.View
          style={[
            animatedFlightClass,
            {
              position: 'absolute',
              height: '100%',
              backgroundColor: 'rgba(52, 52, 52, 0.8)',
              zIndex: 10,
              width: '100%',
              justifyContent: 'flex-start',
              elevation: 4,
            },
          ]}>
          <FlightClassComponent
            onShowFlightClass={onShowFlightClass}
            {...state}
            onChoiceFlightClass={onChoiceFlightClass}
          />
        </Animated.View>
        {/* Another Component */}
        <View
          style={{
            backgroundColor: 'white',
            paddingLeft: 10,
            paddingRight: 10,
            paddingBottom: 200,
          }}>
          <Text
            style={{
              fontWeight: 'bold',
              textAlign: 'center',
              fontSize: 18,
              color: 'rgba(104,113,118,1.00)',
            }}>
            Dapatkan yang Terbaik di Tangan Anda
          </Text>
          <TouchableOpacity
            activeOpacity={0.8}
            style={{
              alignSelf: 'center',
              marginTop: 10,
              marginBottom: 10,
            }}>
            <TravelokaApp />
          </TouchableOpacity>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              marginBottom: 10,
            }}>
            <View>
              <Text
                style={{
                  fontWeight: 'bold',
                  fontSize: 17,
                }}>
                Penting untuk Perjalanan Anda
              </Text>
              <Text
                style={{
                  fontWeight: 'bold',
                  color: 'rgba(104,113,118,1.00)',
                }}>
                Harus bepergian? Pesan Tes COVID-19 di sini!
              </Text>
            </View>
            <TouchableOpacity>
              <ArrowRight width={20} height={20} fill="blue" />
            </TouchableOpacity>
          </View>
          {/*  */}
          <FlatList
            contentContainerStyle={{
              marginBottom: 10,
            }}
            horizontal={true}
            data={TravelingArray}
            renderItem={TravelArray}
            keyExtractor={(item) => item.name}
          />
          <View>
            <Text
              style={{
                fontWeight: 'bold',
                fontSize: 17,
              }}>
              Dapat dikunjungi oleh semua wisatawan
            </Text>
            <Text
              style={{
                color: 'rgba(104,113,118,1.00)',
                fontWeight: 'bold',
              }}>
              Khusus Taiwan, diwajibkan 14 hari karantina setelah kedatangan
            </Text>
          </View>
          <FlatList
            contentContainerStyle={{
              marginTop: 10,
            }}
            horizontal={true}
            data={ImageChild}
            renderItem={ImageChildArray}
            keyExtractor={(item) => item.name}
          />
          <View
            style={{
              width: 80,
              height: 80,
              alignSelf: 'center',
              marginTop: 15,
            }}>
            <Image
              source={{
                uri:
                  'https://ik.imagekit.io/tvlk/image/imageResource/2017/05/10/1494412083831-adecd6744e19478b254cefe2622c52e6.png?tr=h-100,q-75,w-100 1x, https://ik.imagekit.io/tvlk/image/imageResource/2017/05/10/1494412083831-adecd6744e19478b254cefe2622c52e6.png?tr=dpr-2,h-100,q-75,w-100 2x, https://ik.imagekit.io/tvlk/image/imageResource/2017/05/10/1494412083831-adecd6744e19478b254cefe2622c52e6.png?tr=dpr-3,h-100,q-75,w-100 3x',
              }}
              style={{flexGrow: 1}}
            />
          </View>
          <Text
            style={{
              textAlign: 'center',
              fontWeight: 'bold',
              fontSize: 19,
            }}>
            Jaminan Aman Transaksi Online
          </Text>
          <Text
            style={{
              textAlign: 'center',
              fontWeight: '600',
            }}>
            Teknologi SSL dari RapidSSL dengan Sertifikat yang terotentikasi
            menjamin privasi dan keamanan transaksi online Anda. Konfirmasi
            instan dan e-tiket dikirim ke email Anda.
          </Text>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default PlaneComponent;
