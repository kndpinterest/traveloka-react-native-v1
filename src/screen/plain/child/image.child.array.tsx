import React from 'react';
import {ImageBackground, Text, View} from 'react-native';
import moment from 'moment';

interface ImageChildArray {
  url: string;
  name: string;
  price: string;
  date: Date;
}

export const ImageChild: ImageChildArray[] = [
  {
    url:
      'https://ik.imagekit.io/tvlk/image/imageResource/2019/03/13/1552465963244-c4dca43942bc996eb2956808affee2c5.jpeg?tr=q-75,w-104 1x, https://ik.imagekit.io/tvlk/image/imageResource/2019/03/13/1552465963244-c4dca43942bc996eb2956808affee2c5.jpeg?tr=dpr-2,q-75,w-104 2x, https://ik.imagekit.io/tvlk/image/imageResource/2019/03/13/1552465963244-c4dca43942bc996eb2956808affee2c5.jpeg?tr=dpr-3,q-75,w-104 3x',
    name: 'Jakarta  ⇄  Bangkok',
    price: 'Rp3.4jt',
    date: new Date(),
  },
  {
    url:
      'https://ik.imagekit.io/tvlk/image/imageResource/2019/12/17/1576562621896-10bf1c197c4daf681b44f1210b067edc.jpeg?tr=q-75,w-104 1x, https://ik.imagekit.io/tvlk/image/imageResource/2019/12/17/1576562621896-10bf1c197c4daf681b44f1210b067edc.jpeg?tr=dpr-2,q-75,w-104 2x, https://ik.imagekit.io/tvlk/image/imageResource/2019/12/17/1576562621896-10bf1c197c4daf681b44f1210b067edc.jpeg?tr=dpr-3,q-75,w-104 3x',
    name: 'Jakarta  ⇄  Male',
    price: 'Rp7.7jt',
    date: new Date(),
  },
  {
    url:
      'https://ik.imagekit.io/tvlk/image/imageResource/2019/03/13/1552466013222-b38b54359350d835a5e093d0707b8a96.jpeg?tr=q-75,w-104 1x, https://ik.imagekit.io/tvlk/image/imageResource/2019/03/13/1552466013222-b38b54359350d835a5e093d0707b8a96.jpeg?tr=dpr-2,q-75,w-104 2x, https://ik.imagekit.io/tvlk/image/imageResource/2019/03/13/1552466013222-b38b54359350d835a5e093d0707b8a96.jpeg?tr=dpr-3,q-75,w-104 3x',
    name: 'Jakarta  ⇄  Dubai',
    price: 'Rp7.5jt',
    date: new Date(),
  },
  {
    url:
      'https://ik.imagekit.io/tvlk/image/imageResource/2019/03/13/1552466042998-9e183842c613dad0b8daf01a8b98ba11.jpeg?tr=q-75,w-104 1x, https://ik.imagekit.io/tvlk/image/imageResource/2019/03/13/1552466042998-9e183842c613dad0b8daf01a8b98ba11.jpeg?tr=dpr-2,q-75,w-104 2x, https://ik.imagekit.io/tvlk/image/imageResource/2019/03/13/1552466042998-9e183842c613dad0b8daf01a8b98ba11.jpeg?tr=dpr-3,q-75,w-104 3x',
    name: 'Jakarta  ⇄  Istanbul',
    price: 'Rp8jt',
    date: new Date(),
  },
  {
    url:
      'https://ik.imagekit.io/tvlk/image/imageResource/2019/05/15/1557909185986-6772dd5a2aa8f2554a146b478c5b50f1.jpeg?tr=q-75,w-104 1x, https://ik.imagekit.io/tvlk/image/imageResource/2019/05/15/1557909185986-6772dd5a2aa8f2554a146b478c5b50f1.jpeg?tr=dpr-2,q-75,w-104 2x, https://ik.imagekit.io/tvlk/image/imageResource/2019/05/15/1557909185986-6772dd5a2aa8f2554a146b478c5b50f1.jpeg?tr=dpr-3,q-75,w-104 3x',
    name: 'Jakarta  ⇄  Ankara',
    price: 'Rp12.9jt',
    date: new Date(),
  },
];

interface FlatListProp {
  index: number;
  item: ImageChildArray;
}

const ImageChildArray = ({index, item}: FlatListProp) => {
  return (
    <View
      style={{
        width: 160,
        height: 200,
        marginRight: 10,
      }}
      key={index}>
      <ImageBackground
        source={{uri: item.url}}
        style={{
          flexGrow: 1,
          justifyContent: 'flex-end',
          alignItems: 'center',
        }}
        imageStyle={{borderRadius: 10}}>
        <View
          style={{
            backgroundColor: 'rgba(0,166,81,1.00)',
            width: 80,
            paddingTop: 5,
            paddingBottom: 5,
            justifyContent: 'center',
            borderTopLeftRadius: 20,
            borderTopRightRadius: 20,
            alignItems: 'center',
          }}>
          <Text
            style={{
              fontWeight: 'bold',
              color: 'white',
            }}>
            {item.price}
          </Text>
        </View>
      </ImageBackground>
      <View>
        <Text
          style={{
            fontWeight: 'bold',
            fontSize: 17,
          }}>
          {item.name}
        </Text>
        <Text
          style={{
            fontWeight: 'bold',
          }}>
          {moment(item.date).format('DD MMMM YYYY')}
        </Text>
      </View>
    </View>
  );
};

export default ImageChildArray;
