import React from 'react';
import {Image, ImageBackground, Text, View} from 'react-native';

interface TravelingArrayState {
  name: string;
  url: string;
  context: string;
  price: string;
}

export const TravelingArray: TravelingArrayState[] = [
  {
    url:
      'https://ik.imagekit.io/tvlk/xpe-asset/AyJ40ZAo1DOyPyKLZ9c3RGQHTP2oT4ZXW+QmPVVkFQiXFSv42UaHGzSmaSzQ8DO5QIbWPZuF+VkYVRk6gh-Vg4ECbfuQRQ4pHjWJ5Rmbtkk=/2001356152093/Klinik%2520Pintar%2520IDI%2520-%2520Nayya%2520Clinic%2520Jakarta%2520-%2520COVID-19%2520Test%2520-%2520Indonesian%2520Citizens%2520%2528WNI%2529%2520Only-a689f59f-2a13-4f7b-82fa-c4042c5ce1ba.jpeg?_src=imagekit&tr=c-at_max,h-344,q-60,w-193 1x, https://ik.imagekit.io/tvlk/xpe-asset/AyJ40ZAo1DOyPyKLZ9c3RGQHTP2oT4ZXW+QmPVVkFQiXFSv42UaHGzSmaSzQ8DO5QIbWPZuF+VkYVRk6gh-Vg4ECbfuQRQ4pHjWJ5Rmbtkk=/2001356152093/Klinik%2520Pintar%2520IDI%2520-%2520Nayya%2520Clinic%2520Jakarta%2520-%2520COVID-19%2520Test%2520-%2520Indonesian%2520Citizens%2520%2528WNI%2529%2520Only-a689f59f-2a13-4f7b-82fa-c4042c5ce1ba.jpeg?_src=imagekit&tr=dpr-2,c-at_max,h-344,q-60,w-193 2x, https://ik.imagekit.io/tvlk/xpe-asset/AyJ40ZAo1DOyPyKLZ9c3RGQHTP2oT4ZXW+QmPVVkFQiXFSv42UaHGzSmaSzQ8DO5QIbWPZuF+VkYVRk6gh-Vg4ECbfuQRQ4pHjWJ5Rmbtkk=/2001356152093/Klinik%2520Pintar%2520IDI%2520-%2520Nayya%2520Clinic%2520Jakarta%2520-%2520COVID-19%2520Test%2520-%2520Indonesian%2520Citizens%2520%2528WNI%2529%2520Only-a689f59f-2a13-4f7b-82fa-c4042c5ce1ba.jpeg?_src=imagekit&tr=dpr-3,c-at_max,h-344,q-60,w-193 3x',
    name: 'Cilandak Barat',
    context:
      'Klinik Pintar IDI - Permata Bunda Bekasi - Tes COVID-19 - Khusus WNI (Warga Negara Indonesia)',
    price: 'Rp 180.000',
  },
  {
    url:
      'https://ik.imagekit.io/tvlk/xpe-asset/AyJ40ZAo1DOyPyKLZ9c3RGQHTP2oT4ZXW+QmPVVkFQiXFSv42UaHGzSmaSzQ8DO5QIbWPZuF+VkYVRk6gh-Vg4ECbfuQRQ4pHjWJ5Rmbtkk=/2001356152185/Klinik%2520Pintar%2520IDI%2520-%2520Amelia%2520Sidoarjo%2520-%2520COVID-19%2520Rapid%2520Test%2520-%2520Indonesian%2520Citizens%2520%2528WNI%2529%2520Only-672e2939-226c-43fc-87a6-18671817e9df.jpeg?_src=imagekit&tr=c-at_max,h-344,q-60,w-193 1x, https://ik.imagekit.io/tvlk/xpe-asset/AyJ40ZAo1DOyPyKLZ9c3RGQHTP2oT4ZXW+QmPVVkFQiXFSv42UaHGzSmaSzQ8DO5QIbWPZuF+VkYVRk6gh-Vg4ECbfuQRQ4pHjWJ5Rmbtkk=/2001356152185/Klinik%2520Pintar%2520IDI%2520-%2520Amelia%2520Sidoarjo%2520-%2520COVID-19%2520Rapid%2520Test%2520-%2520Indonesian%2520Citizens%2520%2528WNI%2529%2520Only-672e2939-226c-43fc-87a6-18671817e9df.jpeg?_src=imagekit&tr=dpr-2,c-at_max,h-344,q-60,w-193 2x, https://ik.imagekit.io/tvlk/xpe-asset/AyJ40ZAo1DOyPyKLZ9c3RGQHTP2oT4ZXW+QmPVVkFQiXFSv42UaHGzSmaSzQ8DO5QIbWPZuF+VkYVRk6gh-Vg4ECbfuQRQ4pHjWJ5Rmbtkk=/2001356152185/Klinik%2520Pintar%2520IDI%2520-%2520Amelia%2520Sidoarjo%2520-%2520COVID-19%2520Rapid%2520Test%2520-%2520Indonesian%2520Citizens%2520%2528WNI%2529%2520Only-672e2939-226c-43fc-87a6-18671817e9df.jpeg?_src=imagekit&tr=dpr-3,c-at_max,h-344,q-60,w-193 3x',
    name: 'Gedangan',
    context:
      'Klinik Pintar IDI - Kyoai Medical Service Jakarta - Tes COVID-19 - Khusus WNI (Warga Negara Indonesia)',
    price: 'Rp 180.000',
  },
  {
    url:
      'https://ik.imagekit.io/tvlk/xpe-asset/AyJ40ZAo1DOyPyKLZ9c3RGQHTP2oT4ZXW+QmPVVkFQiXFSv42UaHGzSmaSzQ8DO5QIbWPZuF+VkYVRk6gh-Vg4ECbfuQRQ4pHjWJ5Rmbtkk=/2001356152095/Klinik%2520Pintar%2520IDI%2520-%2520JAAC%2520Jakarta%2520-%2520COVID-19%2520Test%2520-%2520Indonesian%2520Citizens%2520%2528WNI%2529%2520Only-fd5ea911-2ea5-445d-9e29-2b24d8c5ff33.jpeg?_src=imagekit&tr=c-at_max,h-344,q-60,w-193 1x, https://ik.imagekit.io/tvlk/xpe-asset/AyJ40ZAo1DOyPyKLZ9c3RGQHTP2oT4ZXW+QmPVVkFQiXFSv42UaHGzSmaSzQ8DO5QIbWPZuF+VkYVRk6gh-Vg4ECbfuQRQ4pHjWJ5Rmbtkk=/2001356152095/Klinik%2520Pintar%2520IDI%2520-%2520JAAC%2520Jakarta%2520-%2520COVID-19%2520Test%2520-%2520Indonesian%2520Citizens%2520%2528WNI%2529%2520Only-fd5ea911-2ea5-445d-9e29-2b24d8c5ff33.jpeg?_src=imagekit&tr=dpr-2,c-at_max,h-344,q-60,w-193 2x, https://ik.imagekit.io/tvlk/xpe-asset/AyJ40ZAo1DOyPyKLZ9c3RGQHTP2oT4ZXW+QmPVVkFQiXFSv42UaHGzSmaSzQ8DO5QIbWPZuF+VkYVRk6gh-Vg4ECbfuQRQ4pHjWJ5Rmbtkk=/2001356152095/Klinik%2520Pintar%2520IDI%2520-%2520JAAC%2520Jakarta%2520-%2520COVID-19%2520Test%2520-%2520Indonesian%2520Citizens%2520%2528WNI%2529%2520Only-fd5ea911-2ea5-445d-9e29-2b24d8c5ff33.jpeg?_src=imagekit&tr=dpr-3,c-at_max,h-344,q-60,w-193 3x',
    name: 'Melawai',
    context:
      'Klinik Pintar IDI - JAAC Jakarta - Tes COVID-19 - Khusus WNI (Warga Negara Indonesia)',
    price: 'Rp 180.000',
  },
  {
    url:
      'https://ik.imagekit.io/tvlk/xpe-asset/AyJ40ZAo1DOyPyKLZ9c3RGQHTP2oT4ZXW+QmPVVkFQiXFSv42UaHGzSmaSzQ8DO5QIbWPZuF+VkYVRk6gh-Vg4ECbfuQRQ4pHjWJ5Rmbtkk=/2001356152125/Klinik%2520Pintar%2520IDI%2520-%2520Indosehat%2520Warakas%2520Jakarta%2520-%2520COVID-19%2520Test%2520-%2520Indonesian%2520Citizens%2520%2528WNI%2529%2520Only-df666cf9-5da0-4e9f-bb60-2b765159e2f9.jpeg?_src=imagekit&tr=c-at_max,h-344,q-60,w-193 1x, https://ik.imagekit.io/tvlk/xpe-asset/AyJ40ZAo1DOyPyKLZ9c3RGQHTP2oT4ZXW+QmPVVkFQiXFSv42UaHGzSmaSzQ8DO5QIbWPZuF+VkYVRk6gh-Vg4ECbfuQRQ4pHjWJ5Rmbtkk=/2001356152125/Klinik%2520Pintar%2520IDI%2520-%2520Indosehat%2520Warakas%2520Jakarta%2520-%2520COVID-19%2520Test%2520-%2520Indonesian%2520Citizens%2520%2528WNI%2529%2520Only-df666cf9-5da0-4e9f-bb60-2b765159e2f9.jpeg?_src=imagekit&tr=dpr-2,c-at_max,h-344,q-60,w-193 2x, https://ik.imagekit.io/tvlk/xpe-asset/AyJ40ZAo1DOyPyKLZ9c3RGQHTP2oT4ZXW+QmPVVkFQiXFSv42UaHGzSmaSzQ8DO5QIbWPZuF+VkYVRk6gh-Vg4ECbfuQRQ4pHjWJ5Rmbtkk=/2001356152125/Klinik%2520Pintar%2520IDI%2520-%2520Indosehat%2520Warakas%2520Jakarta%2520-%2520COVID-19%2520Test%2520-%2520Indonesian%2520Citizens%2520%2528WNI%2529%2520Only-df666cf9-5da0-4e9f-bb60-2b765159e2f9.jpeg?_src=imagekit&tr=dpr-3,c-at_max,h-344,q-60,w-193 3x',
    name: 'Warakas',
    context:
      'Klinik Pintar IDI - Indosehat Warakas Jakarta - Tes COVID-19 - Khusus WNI (Warga Negara Indonesia)',
    price: 'Rp 180.000',
  },
  {
    url:
      'https://ik.imagekit.io/tvlk/xpe-asset/AyJ40ZAo1DOyPyKLZ9c3RGQHTP2oT4ZXW+QmPVVkFQiXFSv42UaHGzSmaSzQ8DO5QIbWPZuF+VkYVRk6gh-Vg4ECbfuQRQ4pHjWJ5Rmbtkk=/2001356152155/Klinik%2520Pintar%2520IDI%2520-%2520Muhammadiyah%2520Sukajadi%2520Bandung%2520-%2520COVID-19%2520Test%2520-%2520Indonesian%2520Citizens%2520%2528WNI%2529%2520Only-9eb2949e-4a00-468c-8781-b2fded8c9cc1.jpeg?_src=imagekit&tr=c-at_max,h-344,q-60,w-193 1x, https://ik.imagekit.io/tvlk/xpe-asset/AyJ40ZAo1DOyPyKLZ9c3RGQHTP2oT4ZXW+QmPVVkFQiXFSv42UaHGzSmaSzQ8DO5QIbWPZuF+VkYVRk6gh-Vg4ECbfuQRQ4pHjWJ5Rmbtkk=/2001356152155/Klinik%2520Pintar%2520IDI%2520-%2520Muhammadiyah%2520Sukajadi%2520Bandung%2520-%2520COVID-19%2520Test%2520-%2520Indonesian%2520Citizens%2520%2528WNI%2529%2520Only-9eb2949e-4a00-468c-8781-b2fded8c9cc1.jpeg?_src=imagekit&tr=dpr-2,c-at_max,h-344,q-60,w-193 2x, https://ik.imagekit.io/tvlk/xpe-asset/AyJ40ZAo1DOyPyKLZ9c3RGQHTP2oT4ZXW+QmPVVkFQiXFSv42UaHGzSmaSzQ8DO5QIbWPZuF+VkYVRk6gh-Vg4ECbfuQRQ4pHjWJ5Rmbtkk=/2001356152155/Klinik%2520Pintar%2520IDI%2520-%2520Muhammadiyah%2520Sukajadi%2520Bandung%2520-%2520COVID-19%2520Test%2520-%2520Indonesian%2520Citizens%2520%2528WNI%2529%2520Only-9eb2949e-4a00-468c-8781-b2fded8c9cc1.jpeg?_src=imagekit&tr=dpr-3,c-at_max,h-344,q-60,w-193 3x',
    name: 'Sukajadi',
    context:
      'Klinik Pintar IDI - Muhammadiyah Sukajadi Bandung - Tes COVID-19 - Khusus WNI (Warga Negara Indonesia)',
    price: 'Rp 180.000',
  },
  {
    url:
      'https://ik.imagekit.io/tvlk/xpe-asset/AyJ40ZAo1DOyPyKLZ9c3RGQHTP2oT4ZXW+QmPVVkFQiXFSv42UaHGzSmaSzQ8DO5QIbWPZuF+VkYVRk6gh-Vg4ECbfuQRQ4pHjWJ5Rmbtkk=/2001356152186/Klinik%2520Pintar%2520IDI%2520-%2520Adi%2520Hayati%2520Surabaya%2520-%2520COVID-19%2520Test%2520-%2520Indonesian%2520Citizens%2520%2528WNI%2529%2520Only-b0584c6d-d4d5-4766-888a-e8bf9834190e.jpeg?_src=imagekit&tr=c-at_max,h-344,q-60,w-193 1x, https://ik.imagekit.io/tvlk/xpe-asset/AyJ40ZAo1DOyPyKLZ9c3RGQHTP2oT4ZXW+QmPVVkFQiXFSv42UaHGzSmaSzQ8DO5QIbWPZuF+VkYVRk6gh-Vg4ECbfuQRQ4pHjWJ5Rmbtkk=/2001356152186/Klinik%2520Pintar%2520IDI%2520-%2520Adi%2520Hayati%2520Surabaya%2520-%2520COVID-19%2520Test%2520-%2520Indonesian%2520Citizens%2520%2528WNI%2529%2520Only-b0584c6d-d4d5-4766-888a-e8bf9834190e.jpeg?_src=imagekit&tr=dpr-2,c-at_max,h-344,q-60,w-193 2x, https://ik.imagekit.io/tvlk/xpe-asset/AyJ40ZAo1DOyPyKLZ9c3RGQHTP2oT4ZXW+QmPVVkFQiXFSv42UaHGzSmaSzQ8DO5QIbWPZuF+VkYVRk6gh-Vg4ECbfuQRQ4pHjWJ5Rmbtkk=/2001356152186/Klinik%2520Pintar%2520IDI%2520-%2520Adi%2520Hayati%2520Surabaya%2520-%2520COVID-19%2520Test%2520-%2520Indonesian%2520Citizens%2520%2528WNI%2529%2520Only-b0584c6d-d4d5-4766-888a-e8bf9834190e.jpeg?_src=imagekit&tr=dpr-3,c-at_max,h-344,q-60,w-193 3x',
    name: 'Wonokromo',
    context:
      'Klinik Pintar IDI - Adi Hayati Surabaya - Tes COVID-19 - Khusus WNI (Warga Negara Indonesia)',
    price: 'Rp 180.000',
  },
  {
    url:
      'https://ik.imagekit.io/tvlk/xpe-asset/AyJ40ZAo1DOyPyKLZ9c3RGQHTP2oT4ZXW+QmPVVkFQiXFSv42UaHGzSmaSzQ8DO5QIbWPZuF+VkYVRk6gh-Vg4ECbfuQRQ4pHjWJ5Rmbtkk=/2001356152152/Klinik%2520Pintar%2520IDI%2520-%2520Permata%2520Bunda%2520Bekasi%2520-%2520COVID-19%2520Test%2520-%2520Indonesian%2520Citizens%2520%2528WNI%2529%2520Only-ce9afc4f-490a-457d-84df-517724b21a7a.jpeg?_src=imagekit&tr=c-at_max,h-344,q-60,w-193 1x, https://ik.imagekit.io/tvlk/xpe-asset/AyJ40ZAo1DOyPyKLZ9c3RGQHTP2oT4ZXW+QmPVVkFQiXFSv42UaHGzSmaSzQ8DO5QIbWPZuF+VkYVRk6gh-Vg4ECbfuQRQ4pHjWJ5Rmbtkk=/2001356152152/Klinik%2520Pintar%2520IDI%2520-%2520Permata%2520Bunda%2520Bekasi%2520-%2520COVID-19%2520Test%2520-%2520Indonesian%2520Citizens%2520%2528WNI%2529%2520Only-ce9afc4f-490a-457d-84df-517724b21a7a.jpeg?_src=imagekit&tr=dpr-2,c-at_max,h-344,q-60,w-193 2x, https://ik.imagekit.io/tvlk/xpe-asset/AyJ40ZAo1DOyPyKLZ9c3RGQHTP2oT4ZXW+QmPVVkFQiXFSv42UaHGzSmaSzQ8DO5QIbWPZuF+VkYVRk6gh-Vg4ECbfuQRQ4pHjWJ5Rmbtkk=/2001356152152/Klinik%2520Pintar%2520IDI%2520-%2520Permata%2520Bunda%2520Bekasi%2520-%2520COVID-19%2520Test%2520-%2520Indonesian%2520Citizens%2520%2528WNI%2529%2520Only-ce9afc4f-490a-457d-84df-517724b21a7a.jpeg?_src=imagekit&tr=dpr-3,c-at_max,h-344,q-60,w-193 3x',
    name: 'Bekasi Timur',
    context:
      'Klinik Pintar IDI - Permata Bunda Bekasi - Tes COVID-19 - Khusus WNI (Warga Negara Indonesia)',
    price: 'Rp 180.000',
  },
  {
    url:
      'https://ik.imagekit.io/tvlk/xpe-asset/AyJ40ZAo1DOyPyKLZ9c3RGQHTP2oT4ZXW+QmPVVkFQiXFSv42UaHGzSmaSzQ8DO5QIbWPZuF+VkYVRk6gh-Vg4ECbfuQRQ4pHjWJ5Rmbtkk=/2001356152130/Klinik%2520Pintar%2520IDI%2520-%2520Kyoai%2520Medical%2520Service%2520Jakarta%2520-%2520COVID-19%2520Test%2520-%2520Indonesian%2520Citizens%2520%2528WNI%2529%2520Only-117d24e7-4485-4ebc-a1ab-8e1605d4336e.jpeg?_src=imagekit&tr=c-at_max,h-344,q-60,w-193 1x, https://ik.imagekit.io/tvlk/xpe-asset/AyJ40ZAo1DOyPyKLZ9c3RGQHTP2oT4ZXW+QmPVVkFQiXFSv42UaHGzSmaSzQ8DO5QIbWPZuF+VkYVRk6gh-Vg4ECbfuQRQ4pHjWJ5Rmbtkk=/2001356152130/Klinik%2520Pintar%2520IDI%2520-%2520Kyoai%2520Medical%2520Service%2520Jakarta%2520-%2520COVID-19%2520Test%2520-%2520Indonesian%2520Citizens%2520%2528WNI%2529%2520Only-117d24e7-4485-4ebc-a1ab-8e1605d4336e.jpeg?_src=imagekit&tr=dpr-2,c-at_max,h-344,q-60,w-193 2x, https://ik.imagekit.io/tvlk/xpe-asset/AyJ40ZAo1DOyPyKLZ9c3RGQHTP2oT4ZXW+QmPVVkFQiXFSv42UaHGzSmaSzQ8DO5QIbWPZuF+VkYVRk6gh-Vg4ECbfuQRQ4pHjWJ5Rmbtkk=/2001356152130/Klinik%2520Pintar%2520IDI%2520-%2520Kyoai%2520Medical%2520Service%2520Jakarta%2520-%2520COVID-19%2520Test%2520-%2520Indonesian%2520Citizens%2520%2528WNI%2529%2520Only-117d24e7-4485-4ebc-a1ab-8e1605d4336e.jpeg?_src=imagekit&tr=dpr-3,c-at_max,h-344,q-60,w-193 3x',
    name: 'Sudirman',
    context:
      'Klinik Pintar IDI - Kyoai Medical Service Jakarta - Tes COVID-19 - Khusus WNI (Warga Negara Indonesia)',
    price: 'Rp 180.000',
  },
  {
    url:
      'https://ik.imagekit.io/tvlk/xpe-asset/AyJ40ZAo1DOyPyKLZ9c3RGQHTP2oT4ZXW+QmPVVkFQiXFSv42UaHGzSmaSzQ8DO5QIbWPZuF+VkYVRk6gh-Vg4ECbfuQRQ4pHjWJ5Rmbtkk=/2001356152247/Klinik%2520Pintar%2520IDI%2520-%2520Bhakti%2520Insani%2520Bekasi%2520-%2520COVID-19%2520Test%2520-%2520Indonesian%2520Citizens%2520%2528WNI%2529%2520Only-30ca19cb-5e98-4103-9e49-54f315afcc81.jpeg?_src=imagekit&tr=c-at_max,h-344,q-60,w-193 1x, https://ik.imagekit.io/tvlk/xpe-asset/AyJ40ZAo1DOyPyKLZ9c3RGQHTP2oT4ZXW+QmPVVkFQiXFSv42UaHGzSmaSzQ8DO5QIbWPZuF+VkYVRk6gh-Vg4ECbfuQRQ4pHjWJ5Rmbtkk=/2001356152247/Klinik%2520Pintar%2520IDI%2520-%2520Bhakti%2520Insani%2520Bekasi%2520-%2520COVID-19%2520Test%2520-%2520Indonesian%2520Citizens%2520%2528WNI%2529%2520Only-30ca19cb-5e98-4103-9e49-54f315afcc81.jpeg?_src=imagekit&tr=dpr-2,c-at_max,h-344,q-60,w-193 2x, https://ik.imagekit.io/tvlk/xpe-asset/AyJ40ZAo1DOyPyKLZ9c3RGQHTP2oT4ZXW+QmPVVkFQiXFSv42UaHGzSmaSzQ8DO5QIbWPZuF+VkYVRk6gh-Vg4ECbfuQRQ4pHjWJ5Rmbtkk=/2001356152247/Klinik%2520Pintar%2520IDI%2520-%2520Bhakti%2520Insani%2520Bekasi%2520-%2520COVID-19%2520Test%2520-%2520Indonesian%2520Citizens%2520%2528WNI%2529%2520Only-30ca19cb-5e98-4103-9e49-54f315afcc81.jpeg?_src=imagekit&tr=dpr-3,c-at_max,h-344,q-60,w-193 3x',
    name: 'Bekasi Barat',
    context:
      'Klinik Pintar IDI - Bhakti Insani Bekasi - Tes COVID-19 - Khusus WNI (Warga Negara Indonesia)',
    price: 'Rp 180.000',
  },
  {
    url:
      'https://ik.imagekit.io/tvlk/xpe-asset/AyJ40ZAo1DOyPyKLZ9c3RGQHTP2oT4ZXW+QmPVVkFQiXFSv42UaHGzSmaSzQ8DO5QIbWPZuF+VkYVRk6gh-Vg4ECbfuQRQ4pHjWJ5Rmbtkk=/2001356152314/Klinik%2520Pintar%2520IDI%2520-%2520Jesslyn%2520Medical%2520Center%2520Jakarta%2520-%2520COVID-19%2520Test%2520-%2520Indonesian%2520Citizens%2520%2528WNI%2529%2520Only-3e5e2fca-843c-4d0d-8219-c34ec7111ab6.jpeg?_src=imagekit&tr=c-at_max,h-344,q-60,w-193 1x, https://ik.imagekit.io/tvlk/xpe-asset/AyJ40ZAo1DOyPyKLZ9c3RGQHTP2oT4ZXW+QmPVVkFQiXFSv42UaHGzSmaSzQ8DO5QIbWPZuF+VkYVRk6gh-Vg4ECbfuQRQ4pHjWJ5Rmbtkk=/2001356152314/Klinik%2520Pintar%2520IDI%2520-%2520Jesslyn%2520Medical%2520Center%2520Jakarta%2520-%2520COVID-19%2520Test%2520-%2520Indonesian%2520Citizens%2520%2528WNI%2529%2520Only-3e5e2fca-843c-4d0d-8219-c34ec7111ab6.jpeg?_src=imagekit&tr=dpr-2,c-at_max,h-344,q-60,w-193 2x, https://ik.imagekit.io/tvlk/xpe-asset/AyJ40ZAo1DOyPyKLZ9c3RGQHTP2oT4ZXW+QmPVVkFQiXFSv42UaHGzSmaSzQ8DO5QIbWPZuF+VkYVRk6gh-Vg4ECbfuQRQ4pHjWJ5Rmbtkk=/2001356152314/Klinik%2520Pintar%2520IDI%2520-%2520Jesslyn%2520Medical%2520Center%2520Jakarta%2520-%2520COVID-19%2520Test%2520-%2520Indonesian%2520Citizens%2520%2528WNI%2529%2520Only-3e5e2fca-843c-4d0d-8219-c34ec7111ab6.jpeg?_src=imagekit&tr=dpr-3,c-at_max,h-344,q-60,w-193 3x',
    name: 'Pegadungan',
    context:
      'Klinik Pintar IDI - Jesslyn Medical Center Jakarta - Tes COVID-19 - Khusus WNI (Warga Negara Indonesia)',
    price: 'Rp 180.000',
  },
];

interface FlatListProp {
  index: number;
  item: TravelingArrayState;
}

const TravelArray = ({index, item}: FlatListProp) => {
  return (
    <View
      style={{
        width: 160,
        height: 200,
        marginRight: 10,
      }}
      key={index}>
      <ImageBackground
        source={{
          uri: item.url,
        }}
        style={{
          flexGrow: 1,
          borderRadius: 10,
          justifyContent: 'flex-end',
          alignItems: 'flex-end',
        }}
        imageStyle={{
          borderRadius: 10,
        }}>
        <View
          style={{
            marginBottom: 10,
            backgroundColor: 'rgba(255,109,112,1.00)',
            width: 120,
            height: 20,
            justifyContent: 'center',
            alignItems: 'center',
            paddingRight: 4,
          }}>
          <Text
            style={{
              fontWeight: 'bold',
              color: 'white',
              alignSelf: 'flex-end',
            }}>
            {item.name}
          </Text>
        </View>
      </ImageBackground>
      <View>
        <Text
          style={{
            fontWeight: 'bold',
          }}
          numberOfLines={2}>
          {item.context}
        </Text>
        <Text
          style={{
            fontWeight: '800',
            color: 'rgba(249,109,1,1.00)',
          }}>
          {item.price}
        </Text>
      </View>
    </View>
  );
};

export default TravelArray;
