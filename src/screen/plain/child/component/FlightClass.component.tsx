import React from 'react';
import {FlatList, Text, TouchableOpacity, View} from 'react-native';
import {PlaneComponentState} from '../..';
import CircleSvg from '../../../../assets/svg/button (1).svg';
import {ContextProps} from '../plane.form';

interface FlightClassComponentState {
  name: string;
  context: string;
}

const FlightClass: FlightClassComponentState[] = [
  {
    name: 'Economy',
    context: 'Meet your primary needs, at the lowest cost.',
  },
  {
    name: 'Premium economy',
    context: 'Affordable travel with delicious food and more space.',
  },
  {
    name: 'Business',
    context: 'Fly comfortably with check-in counters and exclusive seats.',
  },
  {
    name: 'First Class',
    context: 'The most luxurious class with the best and personal service.',
  },
];

interface FlatListProp {
  index: number;
  item: FlightClassComponentState;
}

interface FlightClassState {
  onChoiceFlightClass(name: string, index: number): void;
}

type FlightClassProp = Pick<ContextProps, 'onShowFlightClass'>;

const FlightClassComponent: React.FC<
  FlightClassProp & PlaneComponentState & FlightClassState
> = (
  props: React.PropsWithChildren<
    FlightClassProp & PlaneComponentState & FlightClassState
  >,
) => {
  function renderItem({index, item}: FlatListProp) {
    return (
      <TouchableOpacity
        style={{
          flexDirection: 'row',
          alignItems: 'center',
        }}
        activeOpacity={0.8}
        key={index}
        onPress={props.onChoiceFlightClass.bind(item, item.name, index)}>
        <CircleSvg
          width={22}
          height={22}
          fill={props.flight_class.index === index ? 'red' : 'black'}
        />
        <View
          style={{
            flexDirection: 'column',
            marginLeft: 10,
          }}>
          <Text
            style={{
              fontWeight: 'bold',
              fontSize: 17,
            }}>
            {item.name}
          </Text>
          <Text
            style={{
              fontWeight: 'bold',
              color: 'rgb(104, 113, 118)',
            }}>
            {item.context}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
  return (
    <View
      style={{
        height: 360,
        backgroundColor: 'white',
        elevation: 4,
        width: '100%',
        borderBottomRightRadius: 40,
        borderBottomLeftRadius: 40,
        justifyContent: 'space-between',
        flexDirection: 'column',
      }}>
      <View
        style={{
          borderWidth: 0.6,
          paddingTop: 20,
          paddingBottom: 20,
        }}>
        <Text
          style={{
            fontWeight: 'bold',
            textTransform: 'uppercase',
            textAlign: 'center',
          }}>
          CHOOSE A FLIGHT CLASS
        </Text>
      </View>
      <View
        style={{
          marginTop: 10,
        }}>
        <FlatList
          contentContainerStyle={{
            width: '80%',
            alignSelf: 'center',
          }}
          data={FlightClass}
          renderItem={renderItem}
          keyExtractor={(item) => item.name}
        />
      </View>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-around',
          marginBottom: 10,
        }}>
        <TouchableOpacity
          style={{
            width: 160,
            height: 40,
            backgroundColor: 'white',
            elevation: 0,
            justifyContent: 'center',
            alignItems: 'center',
          }}
          onPress={props.onShowFlightClass}>
          <Text
            style={{
              color: 'rgb(1, 148, 243)',
              fontWeight: 'bold',
              fontSize: 17,
            }}>
            Cancel
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            width: 160,
            height: 40,
            backgroundColor: 'rgb(1, 148, 243)',
            borderRadius: 10,
            elevation: 4,
            justifyContent: 'center',
            alignItems: 'center',
          }}
          onPress={props.onShowFlightClass}>
          <Text
            style={{
              fontSize: 17,
              color: 'white',
              fontWeight: 'bold',
            }}>
            Choose
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default FlightClassComponent;
