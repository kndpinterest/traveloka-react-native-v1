import React from 'react';
import {
  GestureResponderEvent,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import AdultSvg from '../../../../assets/svg/adult.svg';
import ChildSvg from '../../../../assets/svg/child.svg';
import BabySvg from '../../../../assets/svg/baby.svg';
import Arrowtop from '../../../../assets/svg/arrow-top-blue.svg';
import Arrowbottom from '../../../../assets/svg/arrow-bottom-blue.svg';
import {ContextProps} from '../plane.form';
import {PlaneComponentState} from '../..';

type Picks = Pick<ContextProps, 'onShowPassenger'>;

interface PassengerComponentState {
  addPassengerAdul(args: GestureResponderEvent): void;
  removePassengerAdult(args: GestureResponderEvent): void;
  addPassengerChild(args: GestureResponderEvent): void;
  removePassengerChild(args: GestureResponderEvent): void;
  addPassengerBaby(args: GestureResponderEvent): void;
  removePassengerBaby(args: GestureResponderEvent): void;
  passengerOnPress(args: GestureResponderEvent): void;
}

const PassengerComponent: React.FC<
  Picks & PassengerComponentState & PlaneComponentState
> = (
  props: React.PropsWithChildren<
    Picks & PassengerComponentState & PlaneComponentState
  >,
) => {
  return (
    <View
      style={{
        height: 300,
        backgroundColor: 'white',
        elevation: 4,
        width: '100%',
        borderBottomRightRadius: 40,
        borderBottomLeftRadius: 40,
        // padding: 20,
        justifyContent: 'space-between',
        flexDirection: 'column',
      }}>
      <View
        style={{
          borderWidth: 0.6,
          borderTopColor: 'transparent',
          borderLeftColor: 'transparent',
          borderRightColor: 'transparent',
          paddingTop: 20,
          paddingBottom: 20,
        }}>
        <Text
          style={{
            textAlign: 'center',
            textTransform: 'uppercase',
            fontWeight: 'bold',
          }}>
          Add Passengers
        </Text>
      </View>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-around',
        }}>
        <View
          style={{
            flexDirection: 'column',
            alignItems: 'center',
            width: 118,
          }}>
          <View
            style={{
              flexDirection: 'column',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontWeight: 'bold',
                fontSize: 17,
              }}>
              Adult
            </Text>
            <Text
              style={{
                fontWeight: 'bold',
                color: 'rgb(104, 113, 118)',
              }}>
              11 Yr and above
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginTop: 20,
            }}>
            <AdultSvg width={20} height={20} />
            <View
              style={{
                flexDirection: 'column',
                alignItems: 'center',
              }}>
              <TouchableOpacity
                activeOpacity={0.8}
                onPress={props.addPassengerAdul}>
                <Arrowtop width={20} height={20} />
              </TouchableOpacity>
              <Text>{props.passenger}</Text>
              <TouchableOpacity
                activeOpacity={0.8}
                onPress={props.removePassengerAdult}>
                <Arrowbottom width={20} height={20} />
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View
          style={{
            flexDirection: 'column',
            alignItems: 'center',
            width: 118,
          }}>
          <View
            style={{
              flexDirection: 'column',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontWeight: 'bold',
                fontSize: 17,
              }}>
              Child
            </Text>
            <Text
              style={{
                fontWeight: 'bold',
                color: 'rgb(104, 113, 118)',
              }}>
              2 - 11 Yr
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginTop: 20,
            }}>
            <ChildSvg width={20} height={20} />
            <View
              style={{
                flexDirection: 'column',
                alignItems: 'center',
              }}>
              <TouchableOpacity
                activeOpacity={0.8}
                onPress={props.addPassengerChild}>
                <Arrowtop width={20} height={20} />
              </TouchableOpacity>
              <Text>{props.passengerChild}</Text>
              <TouchableOpacity
                activeOpacity={0.8}
                onPress={props.removePassengerChild}>
                <Arrowbottom width={20} height={20} />
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View
          style={{
            flexDirection: 'column',
            alignItems: 'center',
            width: 118,
          }}>
          <View
            style={{
              flexDirection: 'column',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontWeight: 'bold',
                fontSize: 17,
              }}>
              Baby
            </Text>
            <Text
              style={{
                fontWeight: 'bold',
                color: 'rgb(104, 113, 118)',
              }}>
              {'<'} 2 Yr
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginTop: 20,
            }}>
            <BabySvg width={20} height={20} />
            <View
              style={{
                flexDirection: 'column',
                alignItems: 'center',
              }}>
              <TouchableOpacity
                activeOpacity={0.8}
                onPress={props.addPassengerBaby}>
                <Arrowtop width={20} height={20} />
              </TouchableOpacity>
              <Text>{props.passengerBaby}</Text>
              <TouchableOpacity
                activeOpacity={0.8}
                onPress={props.removePassengerBaby}>
                <Arrowbottom width={20} height={20} />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-around',
          marginBottom: 10,
        }}>
        <TouchableOpacity
          style={{
            width: 160,
            height: 40,
            backgroundColor: 'white',
            elevation: 0,
            justifyContent: 'center',
            alignItems: 'center',
          }}
          onPress={props.onShowPassenger}>
          <Text
            style={{
              color: 'rgb(1, 148, 243)',
              fontWeight: 'bold',
              fontSize: 17,
            }}>
            Cancel
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          activeOpacity={0.8}
          style={{
            width: 160,
            height: 40,
            backgroundColor: 'rgb(1, 148, 243)',
            borderRadius: 10,
            elevation: 4,
            justifyContent: 'center',
            alignItems: 'center',
          }}
          onPress={props.passengerOnPress}>
          <Text
            style={{
              fontSize: 17,
              color: 'white',
              fontWeight: 'bold',
            }}>
            Choose
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default PassengerComponent;
