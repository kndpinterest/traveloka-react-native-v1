import {Button} from 'native-base';
import React from 'react';
import {
  GestureResponderEvent,
  NativeSyntheticEvent,
  Switch,
  Text,
  TextInput,
  TextInputChangeEventData,
  TouchableOpacity,
  View,
} from 'react-native';
import moment from 'moment';
import PlaneSvg from '../../../assets/svg/plane.svg';
import PlaneDownSvg from '../../../assets/svg/planedown.svg';
import DateSvg from '../../../assets/svg/dateplane.svg';
import PassengerSvg from '../../../assets/svg/passengerplane.svg';
import ChairSvg from '../../../assets/svg/chairplane.svg';
import DatePicker, {
  WindowsDatePickerChangeEvent,
} from '@react-native-community/datetimepicker';
import {PlaneComponentState, today} from '..';

interface PlaneModalContextState {
  openPassenger: boolean;
  openFlightClass: boolean;
}

export interface ContextProps {
  open: PlaneModalContextState;
  animatedPassenger: any;
  animatedFlightClass: any;
  onShowPassenger(args: GestureResponderEvent): void;
  onShowFlightClass(args: GestureResponderEvent): void;
}

interface PlaneFormState {
  state: PlaneComponentState;
  onShow(): void;
  onChangeFrom(args: NativeSyntheticEvent<TextInputChangeEventData>): void;
  onChangeTo(args: NativeSyntheticEvent<TextInputChangeEventData>): void;
  handleClickRoundTrip(args: boolean): void;
  onDateChange(
    args: WindowsDatePickerChangeEvent | any,
    date?: Date | any,
  ): void;
  onShowPassenger(args: GestureResponderEvent): void;
  onShowFlightClass(args: GestureResponderEvent): void;
  onPress() : void;
}

const PlaneFormComponent: React.FC<PlaneFormState> = (
  props: React.PropsWithChildren<PlaneFormState>,
) => {
  const {
    state,
    onShow,
    onChangeFrom,
    onChangeTo,
    onDateChange,
    handleClickRoundTrip,
    onShowPassenger,
    onShowFlightClass,
    onPress
  } = props;
  return (
    <View>
      <View
        style={{
          marginBottom: 10,
        }}>
        <Text style={{color: 'black', fontWeight: 'bold'}}>From</Text>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            backgroundColor: 'white',
            elevation: 4,
            paddingLeft: 15,
            paddingRight: 15,
            borderRadius: 10,
            marginTop: 5,
            height: 40,
          }}>
          <PlaneSvg width={20} height={20} style={{marginRight: 5}} />
          <TextInput
            value={state.from}
            onChange={onChangeFrom}
            style={{
              fontWeight: 'bold',
              width: '90%',
            }}
            placeholder="From"
            placeholderTextColor="black"
          />
        </View>
      </View>
      <View
        style={{
          marginBottom: 10,
        }}>
        <Text style={{color: 'black', fontWeight: 'bold'}}>To</Text>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            backgroundColor: 'white',
            elevation: 4,
            paddingLeft: 15,
            paddingRight: 15,
            borderRadius: 10,
            marginTop: 5,
            height: 40,
          }}>
          <PlaneDownSvg width={20} height={20} style={{marginRight: 5}} />
          <TextInput
            value={state.to}
            onChange={onChangeTo}
            style={{
              fontWeight: 'bold',
              width: '90%',
            }}
            placeholder="To"
            placeholderTextColor="black"
          />
        </View>
      </View>
      {/* Date Away */}
      <View
        style={{
          marginBottom: 10,
        }}>
        <Text style={{color: 'black', fontWeight: 'bold'}}>Date away</Text>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              backgroundColor: 'white',
              elevation: 4,
              paddingLeft: 15,
              paddingRight: 15,
              borderRadius: 10,
              marginTop: 5,
              height: 40,
            }}>
            <DateSvg width={20} height={20} style={{marginRight: 5}} />
            <TouchableOpacity
              onPress={onShow}
              style={{
                width: '62%',
              }}>
              <Text
                style={{
                  width: '100%',
                  flexWrap: 'nowrap',
                  fontWeight: 'bold',
                }}
                numberOfLines={1}>
                {moment(state.date_away).format('DD-MMMM-YYYY').toString()}
              </Text>
            </TouchableOpacity>
            {state.show ? (
              <DatePicker
                testID="dateTimePicker"
                value={state.date_away}
                minimumDate={
                  new Date(
                    today.getFullYear(),
                    today.getMonth(),
                    today.getDate(),
                  )
                }
                mode="date"
                is24Hour={true}
                display="default"
                onChange={onDateChange}
              />
            ) : null}
          </View>
          <View
            style={{
              flexDirection: 'column',
              justifyContent: 'flex-start',
              marginLeft: 10,
            }}>
            <Text
              style={{
                color: 'black',
                fontWeight: 'bold',
              }}>
              Round trip
            </Text>
            <Switch
              value={state.round_trip}
              onValueChange={handleClickRoundTrip}
              style={{
                alignSelf: 'flex-start',
              }}
            />
          </View>
        </View>
      </View>
      {/* Add Round Trup Date Away */}
      {state.round_trip ? (
        <View
          style={{
            marginBottom: 10,
          }}>
          <Text style={{color: 'black', fontWeight: 'bold'}}>Date away</Text>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                backgroundColor: 'white',
                elevation: 4,
                paddingLeft: 15,
                paddingRight: 15,
                borderRadius: 10,
                marginTop: 5,
                height: 40,
              }}>
              <DateSvg width={20} height={20} style={{marginRight: 5}} />
              <TouchableOpacity
                onPress={onShow}
                style={{
                  width: '62%',
                }}>
                <Text
                  style={{
                    width: '100%',
                    flexWrap: 'nowrap',
                    fontWeight: 'bold',
                  }}
                  numberOfLines={1}>
                  {moment(state.return_date).format('DD-MMMM-YYYY').toString()}
                </Text>
              </TouchableOpacity>
              {state.show ? (
                <DatePicker
                  testID="dateTimePicker"
                  value={state.date_away}
                  minimumDate={
                    new Date(
                      today.getFullYear(),
                      today.getMonth(),
                      today.getDate(),
                    )
                  }
                  mode="date"
                  is24Hour={true}
                  display="default"
                  onChange={onDateChange}
                />
              ) : null}
            </View>
          </View>
        </View>
      ) : null}
      {/*  */}
      <View
        style={{
          marginBottom: 10,
        }}>
        <Text style={{color: 'black', fontWeight: 'bold'}}>Passenger</Text>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            backgroundColor: 'white',
            elevation: 4,
            paddingLeft: 15,
            paddingRight: 15,
            borderRadius: 10,
            marginTop: 5,
            height: 40,
          }}>
          <PassengerSvg width={20} height={20} style={{marginRight: 5}} />
          <TouchableOpacity
            onPress={onShowPassenger}
            style={{
              width: '90%',
            }}>
            <Text
              style={{
                width: '100%',
                flexWrap: 'nowrap',
                fontWeight: 'bold',
              }}
              numberOfLines={1}>
              {state.flightPassenger} Passenger
            </Text>
          </TouchableOpacity>
        </View>
      </View>
      <View
        style={{
          marginBottom: 10,
        }}>
        <Text style={{color: 'black', fontWeight: 'bold'}}>Flight class</Text>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            backgroundColor: 'white',
            elevation: 4,
            paddingLeft: 15,
            paddingRight: 15,
            borderRadius: 10,
            marginTop: 5,
            height: 40,
          }}>
          <ChairSvg width={20} height={20} style={{marginRight: 5}} />
          <TouchableOpacity
            onPress={onShowFlightClass}
            style={{
              width: '90%',
            }}>
            <Text
              style={{
                width: '100%',
                flexWrap: 'nowrap',
                fontWeight: 'bold',
              }}
              numberOfLines={1}>
              {state.flight_class.name}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
      <Button
        style={{
          width: '100%',
          height: 40,
          backgroundColor: 'rgba(255,94,31,1.00)',
          borderRadius: 10,
          elevation: 4,
          justifyContent: 'center',
        }} onPress={onPress}>
        <Text
          style={{
            color: 'white',
            fontWeight: 'bold',
            fontSize: 17,
          }}>
          Find
        </Text>
      </Button>
    </View>
  );
};

export default PlaneFormComponent;
