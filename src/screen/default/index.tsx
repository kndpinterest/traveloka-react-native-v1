import React from 'react';
import {SafeAreaView, ScrollView, Text, View} from 'react-native';
import {NavigationProp} from '../../routes/composite';
import HeaderComponent from './component/header.component';
import ContentV1Component from './component/prefix/contentv1.component';
import ContentV2Component from './component/prefix/contentv2.component';

const DefaultComponent: React.FC<NavigationProp> = (
  props: React.PropsWithChildren<NavigationProp>,
) => {
  return (
    <SafeAreaView
      style={{
        flexGrow: 1,
        backgroundColor: 'white',
      }}>
      <View
        style={{
          flexGrow: 1,
        }}>
        <ScrollView>
          <HeaderComponent />
          <ContentV1Component {...props} />
          <ContentV2Component />
        </ScrollView>
      </View>
    </SafeAreaView>
  );
};

export default DefaultComponent;
