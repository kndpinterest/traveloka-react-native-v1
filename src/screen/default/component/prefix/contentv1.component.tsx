import React from 'react';
import Svg from 'react-native-svg';
import BusSvg from '../../../../assets/svg/bus.svg';
import TrainSvg from '../../../../assets/svg/train.svg';
import JRPessSvg from '../../../../assets/svg/train (1).svg';
import RentalCars from '../../../../assets/svg/sports-car.svg';
import PromoSvg from '../../../../assets/svg/price-tag.svg';
import TravelCars from '../../../../assets/svg/car-of-hatchback-model.svg';
import PesawatSvg from '../../../../assets/svg/air-freight.svg';
import HotelSvg from '../../../../assets/svg/hotel 2.svg';
import GuildeSvg from '../../../../assets/svg/guide.svg';
import GiftSvg from '../../../../assets/svg/giftbox.svg';
import Experience from '../../../../assets/svg/team.svg';
import {FlatList, Text, TouchableOpacity, View} from 'react-native';
import {DrawerRouteName} from '../../../../routes/drawer';
import {NavigationProp} from '../../../../routes/composite';

interface ContentV1Prop {
  name: string;
  icon: React.ReactElement<Svg>;
  backcgorund: string;
  router: DrawerRouteName;
}

const Content: ContentV1Prop[] = [
  {
    name: 'Tiket Pesawat',
    icon: <PesawatSvg width={30} height={30} fill="white" />,
    backcgorund: 'rgba(48,197,247,1.00)',
    router: 'plain',
  },
  {
    name: 'Hotel',
    icon: <HotelSvg width={30} height={30} fill="white" />,
    backcgorund: 'rgba(35,93,159,1.00)',
    router: 'plain',
  },
  {
    name: 'Experience',
    icon: <Experience width={30} height={30} fill="white" />,
    backcgorund: 'rgba(255,109,106,1.00)',
    router: 'plain',
  },
  {
    name: 'Tiket Bus & Travel',
    icon: <BusSvg width={30} height={30} fill="white" />,
    backcgorund: 'rgba(32,191,85,1.00)',
    router: 'plain',
  },
  {
    name: 'Tiket Kerate Api',
    icon: <TrainSvg width={30} height={30} fill="white" />,
    backcgorund: 'rgba(252,160,0,1.00)',
    router: 'plain',
  },
  {
    name: 'JR Pass',
    icon: <JRPessSvg width={30} height={30} fill="white" />,
    backcgorund: 'rgba(252,160,0,1.00)',
    router: 'plain',
  },
  {
    name: 'Rental Mobil',
    icon: <RentalCars width={30} height={30} fill="white" />,
    backcgorund: 'rgba(8,126,139,1.00)',
    router: 'plain',
  },
  {
    name: 'Antar Jemput Bandara',
    icon: <TravelCars width={30} height={30} fill="white" />,
    backcgorund: 'rgba(109,211,206,1.00)',
    router: 'plain',
  },
  {
    name: 'Gift Voucher',
    icon: <GiftSvg width={30} height={30} fill="white" />,
    backcgorund: 'rgba(236,58,62,1.00)',
    router: 'plain',
  },
  {
    name: 'Promo',
    icon: <PromoSvg width={30} height={30} fill="white" />,
    backcgorund: 'rgba(244,85,90,1.00)',
    router: 'plain',
  },
  {
    name: 'Panduan Wisata',
    icon: <GuildeSvg width={30} height={30} fill="white" />,
    backcgorund: 'rgba(164,211,66,1.00)',
    router: 'plain',
  },
];

interface FlatListProp {
  index: number;
  item: ContentV1Prop;
}

const ContentV1Component: React.FC<NavigationProp> = (
  props: React.PropsWithChildren<NavigationProp>,
) => {
  const onRouter = (args: DrawerRouteName) => {
    props.navigation.navigate(args);
  };
  function renderItem({index, item}: FlatListProp) {
    return (
      <TouchableOpacity
        activeOpacity={0.8}
        style={{
          width: 85,
          justifyContent: 'center',
          alignItems: 'center',
          marginTop: 5,
          marginRight: 5,
          marginLeft: 5,
          marginBottom: 20,
        }}
        key={index}
        onPress={onRouter.bind(item, item.router)}>
        <View
          style={{
            alignSelf: 'center',
            width: 50,
            height: 50,
            backgroundColor: item.backcgorund,
            elevation: 5,
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius: 80,
            marginBottom: 5,
          }}>
          {item.icon}
        </View>
        <View
          style={{
            height: 40,
          }}>
          <Text
            style={{
              textAlign: 'center',
              fontSize: 15,
              color: 'rgba(104,113,118,1.00)',
              fontWeight: 'bold',
            }}>
            {item.name}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
  return (
    <FlatList
      numColumns={4}
      data={Content}
      renderItem={renderItem}
      keyExtractor={(item) => item.name}
    />
  );
};

export default ContentV1Component;
