import React from 'react';
import {
  Dimensions,
  FlatList,
  Image,
  ImageBackground,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Svg from 'react-native-svg';
import PlaneHotelSvg from '../../../../assets/svg/holiday (1).svg';
import LoyalitySvg from '../../../../assets/svg/wallet (1).svg';
import CreditCardSvg from '../../../../assets/svg/credit-card.svg';
import PayLater from '../../../../assets/svg/pay (1).svg';
import SignalSvg from '../../../../assets/svg/signal.svg';
import SignalGlobal from '../../../../assets/svg/global.svg';
import Insurence from '../../../../assets/svg/safe.svg';
import TraveloSvg_ from '../../../../assets/svg/travelokaSvg.svg';
import ArrowRight from '../../../../assets/svg/right-arrow (1).svg';

interface ContentV2 {
  name: string;
  icon: React.ReactElement<Svg>;
}

const ContentV2: ContentV2[] = [
  {
    name: 'Pesawat + Hotel',
    icon: <PlaneHotelSvg width={35} height={35} fill="white" />,
  },
  {
    name: 'Loyality Points',
    icon: <LoyalitySvg width={35} height={35} fill="white" />,
  },
  {
    name: 'Mandiri Card',
    icon: <CreditCardSvg width={35} height={35} fill="white" />,
  },
  {
    name: 'PayLater',
    icon: <PayLater width={35} height={35} fill="white" />,
  },
  {
    name: 'Pulsa & Paket Internet',
    icon: <SignalSvg width={35} height={35} fill="white" />,
  },
  {
    name: 'Internet Luar Negeri',
    icon: <SignalGlobal width={35} height={35} fill="white" />,
  },
  {
    name: 'Asuransi',
    icon: <Insurence width={35} height={35} fill="white" />,
  },
];

interface FlatListProp {
  index: number;
  item: ContentV2;
}

const imageArray = [
  'https://ik.imagekit.io/tvlk/image/imageResource/2019/07/23/1563868152031-4af2419c96b9f8fd16a429a8d27adc19.png?tr=w-345%201x,%20https://ik.imagekit.io/tvlk/image/imageResource/2019/07/23/1563868152031-4af2419c96b9f8fd16a429a8d27adc19.png?tr=dpr-2,w-345%202x,%20https://ik.imagekit.io/tvlk/image/imageResource/2019/07/23/1563868152031-4af2419c96b9f8fd16a429a8d27adc19.png?tr=dpr-3,w-345%203x',
  'https://ik.imagekit.io/tvlk/image/imageResource/2021/01/22/1611301828323-09592da0d49bbf10d346d0f7f566fbad.jpeg?tr=q-75,w-345 1x, https://ik.imagekit.io/tvlk/image/imageResource/2021/01/22/1611301828323-09592da0d49bbf10d346d0f7f566fbad.jpeg?tr=dpr-2,q-75,w-345 2x, https://ik.imagekit.io/tvlk/image/imageResource/2021/01/22/1611301828323-09592da0d49bbf10d346d0f7f566fbad.jpeg?tr=dpr-3,q-75,w-345 3x',
  'https://ik.imagekit.io/tvlk/image/imageResource/2021/01/18/1610957190255-c34f549c91264a604cc486b7382ad965.png?tr=q-75,w-345 1x, https://ik.imagekit.io/tvlk/image/imageResource/2021/01/18/1610957190255-c34f549c91264a604cc486b7382ad965.png?tr=dpr-2,q-75,w-345 2x, https://ik.imagekit.io/tvlk/image/imageResource/2021/01/18/1610957190255-c34f549c91264a604cc486b7382ad965.png?tr=dpr-3,q-75,w-345 3x',
  'https://ik.imagekit.io/tvlk/image/imageResource/2021/01/08/1610094800733-7bf35500db56fa2d62fc1fe5f487839a.jpeg?tr=q-75,w-345 1x, https://ik.imagekit.io/tvlk/image/imageResource/2021/01/08/1610094800733-7bf35500db56fa2d62fc1fe5f487839a.jpeg?tr=dpr-2,q-75,w-345 2x, https://ik.imagekit.io/tvlk/image/imageResource/2021/01/08/1610094800733-7bf35500db56fa2d62fc1fe5f487839a.jpeg?tr=dpr-3,q-75,w-345 3x',
  'https://ik.imagekit.io/tvlk/image/imageResource/2021/01/08/1610126279040-5d0ceb0ae154bc6b6a8335ae088910e2.jpeg?tr=q-75,w-345 1x, https://ik.imagekit.io/tvlk/image/imageResource/2021/01/08/1610126279040-5d0ceb0ae154bc6b6a8335ae088910e2.jpeg?tr=dpr-2,q-75,w-345 2x, https://ik.imagekit.io/tvlk/image/imageResource/2021/01/08/1610126279040-5d0ceb0ae154bc6b6a8335ae088910e2.jpeg?tr=dpr-3,q-75,w-345 3x',
];

const imageDestinasi = [
  'https://ik.imagekit.io/tvlk/image/imageResource/2018/09/21/1537503984416-c854f76600406bfed46ee679f6ca7272.jpeg?tr=h-180,q-75 1x, https://ik.imagekit.io/tvlk/image/imageResource/2018/09/21/1537503984416-c854f76600406bfed46ee679f6ca7272.jpeg?tr=dpr-2,h-180,q-75 2x, https://ik.imagekit.io/tvlk/image/imageResource/2018/09/21/1537503984416-c854f76600406bfed46ee679f6ca7272.jpeg?tr=dpr-3,h-180,q-75 3x',
  'https://ik.imagekit.io/tvlk/image/imageResource/2018/09/21/1537503993818-aeda025f1808f162446f12634345889b.jpeg?tr=h-180,q-75 1x, https://ik.imagekit.io/tvlk/image/imageResource/2018/09/21/1537503993818-aeda025f1808f162446f12634345889b.jpeg?tr=dpr-2,h-180,q-75 2x, https://ik.imagekit.io/tvlk/image/imageResource/2018/09/21/1537503993818-aeda025f1808f162446f12634345889b.jpeg?tr=dpr-3,h-180,q-75 3x',
  'https://ik.imagekit.io/tvlk/image/imageResource/2018/09/21/1537504002100-b56bcb9c4df45d40b03054c95205a9d0.jpeg?tr=h-180,q-75 1x, https://ik.imagekit.io/tvlk/image/imageResource/2018/09/21/1537504002100-b56bcb9c4df45d40b03054c95205a9d0.jpeg?tr=dpr-2,h-180,q-75 2x, https://ik.imagekit.io/tvlk/image/imageResource/2018/09/21/1537504002100-b56bcb9c4df45d40b03054c95205a9d0.jpeg?tr=dpr-3,h-180,q-75 3x',
  'https://ik.imagekit.io/tvlk/image/imageResource/2018/09/21/1537504009433-f8d140b2040657537ee60b01e4a3ba29.jpeg?tr=h-180,q-75 1x, https://ik.imagekit.io/tvlk/image/imageResource/2018/09/21/1537504009433-f8d140b2040657537ee60b01e4a3ba29.jpeg?tr=dpr-2,h-180,q-75 2x, https://ik.imagekit.io/tvlk/image/imageResource/2018/09/21/1537504009433-f8d140b2040657537ee60b01e4a3ba29.jpeg?tr=dpr-3,h-180,q-75 3x',
  'https://ik.imagekit.io/tvlk/image/imageResource/2018/09/21/1537504015347-4dc4ec71da78d35cf163930e652c8451.jpeg?tr=h-180,q-75 1x, https://ik.imagekit.io/tvlk/image/imageResource/2018/09/21/1537504015347-4dc4ec71da78d35cf163930e652c8451.jpeg?tr=dpr-2,h-180,q-75 2x, https://ik.imagekit.io/tvlk/image/imageResource/2018/09/21/1537504015347-4dc4ec71da78d35cf163930e652c8451.jpeg?tr=dpr-3,h-180,q-75 3x',
  'https://ik.imagekit.io/tvlk/image/imageResource/2018/09/21/1537504019852-749fc278709155ac2cafb22ea8d47d05.jpeg?tr=h-180,q-75 1x, https://ik.imagekit.io/tvlk/image/imageResource/2018/09/21/1537504019852-749fc278709155ac2cafb22ea8d47d05.jpeg?tr=dpr-2,h-180,q-75 2x, https://ik.imagekit.io/tvlk/image/imageResource/2018/09/21/1537504019852-749fc278709155ac2cafb22ea8d47d05.jpeg?tr=dpr-3,h-180,q-75 3x',
  'https://ik.imagekit.io/tvlk/image/imageResource/2018/09/21/1537504026999-459bb731a59a6bd3bc0534cf92c48b68.jpeg?tr=h-180,q-75 1x, https://ik.imagekit.io/tvlk/image/imageResource/2018/09/21/1537504026999-459bb731a59a6bd3bc0534cf92c48b68.jpeg?tr=dpr-2,h-180,q-75 2x, https://ik.imagekit.io/tvlk/image/imageResource/2018/09/21/1537504026999-459bb731a59a6bd3bc0534cf92c48b68.jpeg?tr=dpr-3,h-180,q-75 3x',
  'https://ik.imagekit.io/tvlk/image/imageResource/2018/09/21/1537504033724-ec85304609a6d811299303ee34936565.jpeg?tr=h-180,q-75 1x, https://ik.imagekit.io/tvlk/image/imageResource/2018/09/21/1537504033724-ec85304609a6d811299303ee34936565.jpeg?tr=dpr-2,h-180,q-75 2x, https://ik.imagekit.io/tvlk/image/imageResource/2018/09/21/1537504033724-ec85304609a6d811299303ee34936565.jpeg?tr=dpr-3,h-180,q-75 3x',
];

interface FlatListImageProp {
  index: number;
  item: string;
}

interface AppartemenState {
  url: string;
  name: string;
  context: string;
}

interface FlatListImageAppartemenProp {
  index: number;
  item: AppartemenState;
}

const appartemen: AppartemenState[] = [
  {
    url:
      'https://ik.imagekit.io/tvlk/image/imageResource/2019/07/03/1562129205655-ca360a552284b78b3a95c3c1bcf48d1c.jpeg?tr=h-200,q-75 1x, https://ik.imagekit.io/tvlk/image/imageResource/2019/07/03/1562129205655-ca360a552284b78b3a95c3c1bcf48d1c.jpeg?tr=dpr-2,h-200,q-75 2x, https://ik.imagekit.io/tvlk/image/imageResource/2019/07/03/1562129205655-ca360a552284b78b3a95c3c1bcf48d1c.jpeg?tr=dpr-3,h-200,q-75 3x',
    name: 'Villa',
    context: '17.000 + Villa',
  },
  {
    url:
      'https://ik.imagekit.io/tvlk/image/imageResource/2019/07/03/1562129201744-7a9fb25b2ae84bc5d7412abf16d5e8eb.jpeg?tr=h-200,q-75 1x, https://ik.imagekit.io/tvlk/image/imageResource/2019/07/03/1562129201744-7a9fb25b2ae84bc5d7412abf16d5e8eb.jpeg?tr=dpr-2,h-200,q-75 2x, https://ik.imagekit.io/tvlk/image/imageResource/2019/07/03/1562129201744-7a9fb25b2ae84bc5d7412abf16d5e8eb.jpeg?tr=dpr-3,h-200,q-75 3x',
    name: 'Appartemen',
    context: '17.000 + Appartemen',
  },
];

const ContentV2Component = () => {
  function renderItem({index, item}: FlatListProp) {
    return (
      <View
        key={index}
        style={{
          marginTop: 5,
          marginBottom: 5,
          marginRight: 10,
          marginLeft: 10,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        {item.icon}
        <Text
          style={{
            marginTop: 5,
            fontSize: 15,
            color: 'rgba(104,113,118,1.00)',
            fontWeight: 'bold',
          }}>
          {item.name}
        </Text>
      </View>
    );
  }

  function renderItemImage({index, item}: FlatListImageProp) {
    return (
      <TouchableOpacity
        key={index}
        style={{
          width: index === 0 ? 120 : 240,
          height: 120,
          marginRight: 10,
        }}>
        <ImageBackground
          source={{uri: item}}
          style={{flexGrow: 1, borderRadius: 5, justifyContent: 'flex-end'}}>
          {index === 0 ? (
            <Text
              style={{
                fontSize: 12,
                color: 'white',
                fontWeight: 'bold',
                textAlign: 'center',
              }}>
              Lihat Semua Promo
            </Text>
          ) : null}
        </ImageBackground>
      </TouchableOpacity>
    );
  }

  function renderItemDestinasi({index, item}: FlatListImageProp) {
    return (
      <View>
        <TouchableOpacity
          key={index}
          style={{
            height: 200,
            width: 160,
            marginRight: 10,
            borderRadius: 10,
          }}>
          <Image source={{uri: item}} style={{flexGrow: 1, borderRadius: 10}} />
        </TouchableOpacity>
      </View>
    );
  }

  function renderItemAppartemen({index, item}: FlatListImageAppartemenProp) {
    return (
      <TouchableOpacity
        key={index}
        style={{
          height: 200,
          width: 160,
          marginRight: 10,
          borderRadius: 10,
        }}>
        <Image
          source={{uri: item.url}}
          style={{flexGrow: 1, borderRadius: 10}}
        />
        <View
          style={{
            flexDirection: 'column',
          }}>
          <Text
            style={{
              fontWeight: 'bold',
            }}>
            {item.name}
          </Text>
          <Text
            style={{
              fontWeight: 'bold',
              color: 'rgb(104, 113, 118)',
            }}>
            {item.context}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
  return (
    <View>
      <ScrollView horizontal={true}>
        <FlatList
          contentContainerStyle={{flexDirection: 'row', alignItems: 'center'}}
          data={ContentV2}
          renderItem={renderItem}
          keyExtractor={(item) => item.name}
        />
      </ScrollView>
      <View
        style={{
          backgroundColor: 'rgba(27,160,226,1.00)',
          padding: 15,
          margin: 15,
          borderRadius: 5,
        }}>
        <View
          style={{
            flexDirection: 'row',
          }}>
          <View
            style={{
              backgroundColor: 'white',
              width: 60,
              height: 60,
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: 5,
            }}>
            <TraveloSvg_ width={50} height={50} />
          </View>
          <View
            style={{
              width: Dimensions.get('screen').width / 1.4,
              marginLeft: 10,
            }}>
            <Text style={{color: 'white', fontSize: 14, fontWeight: 'bold'}}>
              Dapatkan yang Terbaik di Tangan Anda
            </Text>
            <Text style={{color: 'white'}}>
              Unduh Traveloka App dan jelajahi seluruh fitur kami untuk
              kebutuhan travel &amp; gaya hidup Anda.
            </Text>
          </View>
        </View>
        <TouchableOpacity
          style={{
            height: 35,
            width: '100%',
            backgroundColor: 'white',
            alignItems: 'center',
            justifyContent: 'center',
            borderRadius: 5,
          }}>
          <Text
            style={{
              fontWeight: 'bold',
              color: 'rgba(1,148,243,1.00)',
            }}>
            Unduh Sekarang
          </Text>
        </TouchableOpacity>
      </View>
      <View
        style={{
          marginTop: 20,
          marginBottom: 20,
          backgroundColor: '#f2f3f3',
          padding: 15,
        }}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}>
          <View
            style={{
              flexDirection: 'column',
            }}>
            <Text
              style={{
                fontWeight: 'bold',
                fontSize: 17,
              }}>
              Promo Saat Ini
            </Text>
            <Text>Selalu bisa hemat dengan ragam penawaran kami</Text>
          </View>
          <TouchableOpacity>
            <ArrowRight width={18} height={18} fill="skyblue" />
          </TouchableOpacity>
        </View>
        <ScrollView
          horizontal={true}
          contentContainerStyle={{
            marginTop: 10,
          }}>
          <FlatList
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            data={imageArray}
            renderItem={renderItemImage}
            keyExtractor={(item) => item}
          />
        </ScrollView>
      </View>
      <View
        style={{
          marginBottom: 20,
          padding: 15,
          backgroundColor: 'white',
          elevation: 4,
        }}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            display: 'flex',
            justifyContent: 'space-between',
            marginBottom: 10,
          }}>
          <Text
            style={{
              fontWeight: 'bold',
              fontSize: 17,
              marginBottom: 10,
            }}>
            Destinasi Populer
          </Text>
          <TouchableOpacity>
            <ArrowRight width={17} height={17} fill="skyblue" />
          </TouchableOpacity>
        </View>
        <ScrollView horizontal={true}>
          <FlatList
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            data={imageDestinasi}
            renderItem={renderItemDestinasi}
            keyExtractor={(item) => item}
          />
        </ScrollView>
      </View>
      <View
        style={{
          padding: 15,
          backgroundColor: 'white',
          elevation: 4,
          marginBottom: 20,
        }}>
        <Text
          style={{
            fontWeight: 'bold',
            fontSize: 17,
            marginBottom: 10,
          }}>
          Ruang lebih untuk keluarga dan kawan
        </Text>
        <Text
          style={{
            color: 'rgb(104, 113, 118)',
            fontWeight: 'bold',
          }}>
          Nikmati berbagai pilihan apartemen dan vila di Traveloka
        </Text>
        <FlatList
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          contentContainerStyle={{
            marginTop: 10,
            marginBottom: 10,
          }}
          data={appartemen}
          renderItem={renderItemAppartemen}
          keyExtractor={(item) => item.url}
        />
      </View>
    </View>
  );
};

export default ContentV2Component;
