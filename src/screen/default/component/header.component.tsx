import React from 'react'
import { Text, View } from 'react-native'
import UserSvg from '../../../assets/svg/user.svg';

const HeaderComponent = () => {
    return (
        <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
          marginTop: 10,
          paddingBottom: 15,
          borderWidth: 0.4,
          marginBottom: 15,
          borderTopColor: 'transparent',
          borderRightColor: 'transparent',
          borderLeftColor: 'transparent'
        }}>
        <View
          style={{
            width: 50,
            height: 50,
            elevation: 4,
            backgroundColor: 'white',
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius: 80,
            marginRight: 10,
          }}>
          <UserSvg width={30} height={30} fill="black" />
        </View>
        <View
          style={{
            flexDirection: 'column',
          }}>
          <Text
            style={{
              fontSize: 20,
              fontWeight: 'bold',
            }}>
              Kenedi Novriansyah Dev
          </Text>
          <Text
            style={{
              fontWeight: 'bold',
            }}>
            Enjoy travelokaPay and cheaper prices
          </Text>
        </View>
      </View>
    )
}

export default HeaderComponent