import {View} from 'native-base';
import React from 'react';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {NavigationProp} from '../../../routes/composite';
import {CredentialsContext, CredentialsContextApp} from './context';

const Credentials: React.FC<NavigationProp> = (
  props: React.PropsWithChildren<NavigationProp>,
) => {
  return (
    <View
      style={{
        flexGrow: 1,
        backgroundColor: 'white',
      }}>
      <KeyboardAwareScrollView
        contentContainerStyle={{
          flexGrow: 1,
        }}>
        <CredentialsContext.Provider
          value={{
            screen: props.route.params.name,
            navigation: props,
          }}>
          <CredentialsContextApp />
        </CredentialsContext.Provider>
      </KeyboardAwareScrollView>
    </View>
  );
};

export default Credentials;
