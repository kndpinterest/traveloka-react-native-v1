import {Text, View} from 'native-base';
import React from 'react';
import {
  NativeSyntheticEvent,
  TextInput,
  TextInputChangeEventData,
} from 'react-native';
import PhoneInput from 'react-native-phone-number-input';
import {RegisterComponentState} from '../register.component';
import CheckBox from '@react-native-community/checkbox';

interface ContextProps {
  state: RegisterComponentState;
  onChangeValue(args: string): void;
  onChangeFormattedText(args: string): void;
  onChangeEmail(args: NativeSyntheticEvent<TextInputChangeEventData>): void;
  onChangeCheckBox(args: boolean): void;
}

export const RegisterContext = React.createContext<ContextProps>({});
export const RegisterContextApp = () => {
  return (
    <RegisterContext.Consumer>
      {({
        state,
        onChangeValue,
        onChangeFormattedText,
        onChangeEmail,
        onChangeCheckBox,
      }) => {
        if (state.open) {
          return (
            <View>
              <TextInput
                style={{
                  height: 40,
                  backgroundColor: 'white',
                  elevation: 4,
                  borderRadius: 10,
                  paddingLeft: 15,
                  paddingRight: 15,
                }}
                value={state.email}
                onChange={onChangeEmail}
                keyboardType="email-address"
                placeholder="Alamat email"
                placeholderTextColor="black"
              />
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginTop: 10
                }}>
                <CheckBox
                  value={state.check}
                  onValueChange={onChangeCheckBox}
                />
                <Text
                  style={{
                    marginLeft: 5,
                    fontWeight: 'bold',
                    fontSize: 14
                  }}>
                  Berlangganan Info Promo
                </Text>
              </View>
            </View>
          );
        } else {
          return (
            <View>
              <PhoneInput
                ref={state.phoneInput}
                defaultValue={state.value}
                defaultCode="DM"
                layout="first"
                onChangeText={onChangeValue}
                onChangeFormattedText={onChangeFormattedText}
                codeTextStyle={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center',
                  height: 24.999,
                }}
                textContainerStyle={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center',
                  height: 40,
                  backgroundColor: 'white',
                  borderRadius: 10,
                }}
                containerStyle={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center',
                  height: 40,
                  width: '100%',
                  backgroundColor: 'white',
                  borderRadius: 10,
                }}
                textInputStyle={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center',
                  height: 40,
                }}
                withDarkTheme
                withShadow
                autoFocus
              />
            </View>
          );
        }
      }}
    </RegisterContext.Consumer>
  );
};
