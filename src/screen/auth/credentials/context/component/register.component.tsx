import {Button} from 'native-base';
import React from 'react';
import {
  Dimensions,
  GestureResponderEvent,
  NativeSyntheticEvent,
  Text,
  TextInputChangeEventData,
  TouchableOpacity,
  View,
} from 'react-native';
import Animated, {
  useAnimatedStyle,
  useSharedValue,
  withSpring,
} from 'react-native-reanimated';
import {ContextProps, CredentialsScreenState} from '..';
import CoinSvg from '../../../../../assets/svg/money.svg';
import Facebook from '../../../../../assets/svg/facebook.svg';
import Google from '../../../../../assets/svg/search.svg';
import PhoneInput from 'react-native-phone-number-input';
import {RegisterContext, RegisterContextApp} from './context/register.context';

type Picks = Pick<ContextProps, 'navigation'>;

const height = -Dimensions.get('screen').height;

export interface RegisterComponentState {
  open: boolean;
  value: string;
  formattedValue: string;
  valid: boolean;
  phoneInput: React.Ref<PhoneInput>;
  email?: string;
  check: boolean;
}

const RegisterComponent: React.FC<Picks> = (
  props: React.PropsWithChildren<Picks>,
) => {
  const offset = useSharedValue(height);
  const animated = useAnimatedStyle(() => {
    return {
      top: withSpring(offset.value),
    };
  });
  React.useEffect(() => {
    let mounted = true;
    if (mounted) {
      offset.value = 0;
    }
    return () => {
      mounted = false;
    };
  }, []);
  const [state, setState] = React.useState<RegisterComponentState>({
    open: false,
    value: '',
    formattedValue: '',
    valid: false,
    phoneInput: null,
    check: false,
  });

  const onSwitchInput = (args: GestureResponderEvent) => {
    args.preventDefault();
    setState({
      ...state,
      open: !state.open,
    });
  };

  const onChangeValue = (args: string) => {
    setState({
      ...state,
      value: args,
    });
  };
  const onChangeFormattedText = (args: string) => {
    setState({
      ...state,
      formattedValue: args,
    });
  };
  const onChangeCheckBox = (args: boolean) => {
    setState({
      ...state,
      check: args,
    });
  };
  const onChangeEmail = (
    args: NativeSyntheticEvent<TextInputChangeEventData>,
  ) => {
    setState({
      ...state,
      email: args.nativeEvent.text,
    });
  };

  const onRouter = (args: CredentialsScreenState) => {
    props.navigation.navigation.navigate('credentials', {
      name: args,
    });
  };
  return (
    <RegisterContext.Provider
      value={{
        state,
        onChangeValue,
        onChangeFormattedText,
        onChangeEmail,
        onChangeCheckBox,
      }}>
      <Animated.View
        style={[
          {
            position: 'absolute',
            width: '100%',
          },
          animated,
        ]}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            paddingLeft: 15,
            paddingRight: 15,
            paddingTop: 20,
            paddingBottom: 20,
            backgroundColor: '#073e68',
          }}>
          <CoinSvg width={20} height={20} />
          <Text
            style={{
              color: 'white',
              fontWeight: 'bold',
              marginLeft: 10,
              textTransform: 'capitalize',
            }}>
            Jadi member untuk mendapat beragam keuntungan dan mulai kumpulkan
            Poin!
          </Text>
        </View>
        <View
          style={{
            marginLeft: 10,
            marginRight: 10,
          }}>
          <Text
            style={{
              fontWeight: 'bold',
              marginTop: 10,
              fontSize: 18,
            }}>
            Buat Akun Member
          </Text>
          <View
            style={{
              marginBottom: 20,
            }}>
            <Text
              style={{
                fontWeight: 'bold',
                marginBottom: 10,
              }}>
              {state.open ? 'Alamat Email' : 'Nomor Handphone'}
            </Text>
            <RegisterContextApp />
          </View>
          <Text
            style={{
              fontWeight: 'bold',
              color: '#8F8F8F',
            }}>
            Gabung jadi member Traveloka untuk menikmati kemudahan pemesanan dan
            berbagai keuntungan!
          </Text>
          <Button
            style={{
              width: '100%',
              justifyContent: 'center',
              backgroundColor: '#f96d01',
              borderRadius: 10,
              marginTop: 10,
            }}>
            <Text
              style={{
                color: 'white',
                fontWeight: 'bold',
                fontSize: 17,
                textTransform: 'uppercase',
              }}>
              Daftar
            </Text>
          </Button>
          <TouchableOpacity onPress={onSwitchInput}>
            <Text
              style={{
                textAlign: 'center',
                fontWeight: 'bold',
                color: '#136cbc',
                marginTop: 15,
              }}>
              Daftar dengan email
            </Text>
          </TouchableOpacity>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginTop: 15,
              marginBottom: 15,
            }}>
            <View
              style={{
                flex: 1,
                height: 0.8,
                backgroundColor: 'rgba(205,208,209,1.00)',
              }}
            />
            <View>
              <Text
                style={{
                  width: 50,
                  textAlign: 'center',
                  color: 'rgba(104,113,118,1.00)',
                }}>
                atau
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                height: 0.8,
                backgroundColor: 'rgba(205,208,209,1.00)',
              }}
            />
          </View>
          <Button
            style={{
              width: '100%',
              marginBottom: 10,
              borderRadius: 10,
              justifyContent: 'space-between',
              backgroundColor: 'rgba(59,89,152,1.00)',
              paddingLeft: 15,
            }}>
            <Facebook width={20} height={20} />
            <Text
              style={{
                marginLeft: 10,
                color: 'white',
                fontWeight: 'bold',
                textTransform: 'capitalize',
                width: 160,
              }}>
              dafter dengan facebook
            </Text>
            <View />
          </Button>
          <Button
            style={{
              width: '100%',
              marginBottom: 10,
              borderRadius: 10,
              justifyContent: 'space-between',
              backgroundColor: 'rgba(66,133,244,1.00)',
              paddingLeft: 15,
            }}>
            <Google width={20} height={20} />
            <Text
              style={{
                marginLeft: 10,
                color: 'white',
                fontWeight: 'bold',
                textTransform: 'capitalize',
                width: 160,
              }}>
              dafter dengan Google
            </Text>
            <View />
          </Button>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: 10,
            }}>
            <Text
              style={{
                fontWeight: 'bold',
              }}>
              Sudah memiliki akun?
            </Text>
            <TouchableOpacity
              style={{
                marginLeft: 10,
              }}
              onPress={onRouter.bind('', 'login')}>
              <Text
                style={{
                  color: 'rgb(1, 148, 243)',
                  fontWeight: 'bold',
                }}>
                Log in
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Animated.View>
    </RegisterContext.Provider>
  );
};

export default RegisterComponent;
