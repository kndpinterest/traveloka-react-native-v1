import {Button, Text, View} from 'native-base';
import React from 'react';
import {
  Dimensions,
  NativeSyntheticEvent,
  TextInput,
  TextInputChangeEventData,
} from 'react-native';
import Animated, {
  useAnimatedStyle,
  useSharedValue,
  withSpring,
} from 'react-native-reanimated';
import {ContextProps} from '..';

const height = -Dimensions.get('screen').height;

type Picks = Pick<ContextProps, 'navigation'>;

interface ForgotComponentState {
  active: boolean;
  value: string;
}

const ForgotComponent: React.FC<Picks> = (
  props: React.PropsWithChildren<Picks>,
) => {
  const offset = useSharedValue(height);
  const animated = useAnimatedStyle(() => {
    return {
      top: withSpring(offset.value),
    };
  });
  React.useEffect(() => {
    let mounted = true;
    if (mounted) {
      offset.value = 0;
    }
    return () => {
      mounted = false;
    };
  }, []);
  const [state, setState] = React.useState<ForgotComponentState>({
    active: false,
    value: '',
  });
  const onChange = (args: NativeSyntheticEvent<TextInputChangeEventData>) => {
    setState({
      ...state,
      value: args.nativeEvent.text,
      active: args.nativeEvent.text.length === 0 ? false : true,
    });
  };
  return (
    <Animated.View
      style={[
        {
          position: 'absolute',
          width: '100%',
        },
        animated,
      ]}>
      <View
        style={{
          padding: 15,
        }}>
        <Text
          style={{
            fontWeight: 'bold',
            fontSize: 20,
          }}>
          Lupa Password
        </Text>
        <Text
          style={{
            color: 'rgba(104,113,118,1.00)',
          }}>
          Silakan masukkan Login ID Anda untuk reset password.
        </Text>
        <View
          style={{
            marginTop: 15,
          }}>
          <Text
            style={{
              fontWeight: 'bold',
              marginBottom: 10,
            }}>
            Email atau No. Handphone
          </Text>
          <TextInput
            value={state.value}
            onChange={onChange}
            style={{
              width: '100%',
              backgroundColor: 'white',
              elevation: 4,
              borderRadius: 10,
              height: 40,
              paddingLeft: 15,
              paddingRight: 15,
            }}
            placeholder="Email atau No, handphone"
          />
        </View>
        <Button
          style={{
            marginTop: 15,
            width: '100%',
            justifyContent: 'center',
            backgroundColor: state.active ? '#f96d01' : '#dadada',
            borderRadius: 10,
          }}
          disabled={state.active}>
          <Text>Lanjutkan</Text>
        </Button>
      </View>
    </Animated.View>
  );
};

export default ForgotComponent;
