import React from 'react';
import {
  Dimensions,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {Button} from 'native-base';
import Animated, {
  useAnimatedStyle,
  useSharedValue,
  withSpring,
} from 'react-native-reanimated';
import EyesSvg from '../../../../../assets/svg/view.svg';
import Facebook from '../../../../../assets/svg/facebook.svg';
import Google from '../../../../../assets/svg/search.svg';
import {ContextProps, CredentialsScreenState} from '../';

const height = -Dimensions.get('screen').height;

type Picks = Pick<ContextProps, 'navigation'>;

const LoginComponent: React.FC<Picks> = (
  props: React.PropsWithChildren<Picks>,
) => {
  const offset = useSharedValue(height);
  const animated = useAnimatedStyle(() => {
    return {
      top: withSpring(offset.value),
    };
  });
  React.useEffect(() => {
    let mounted = true;
    if (mounted) {
      offset.value = 0;
    }
    return () => {
      mounted = false;
    };
  }, []);

  const onRouter = (args: CredentialsScreenState) => {
    props.navigation.navigation.navigate('credentials', {
      name: args,
    });
  };
  return (
    <Animated.View
      style={[
        animated,
        {
          position: 'absolute',
          marginRight: 10,
          marginLeft: 10,
        },
      ]}>
      <Text
        style={{
          fontSize: 25,
          fontWeight: 'bold',
          textAlign: 'center',
        }}>
        Log in
      </Text>
      <Text
        style={{
          fontSize: 15,
          fontWeight: 'bold',
          color: 'rgba(104,113,118,1.00)',
        }}>
        Log in ke akun Traveloka Anda untuk kemudahan pemesanan.
      </Text>
      <View
        style={{
          marginTop: 20,
        }}>
        <View
          style={{
            marginBottom: 10,
          }}>
          <Text
            style={{
              fontWeight: 'bold',
            }}>
            Email atau No. Handphone
          </Text>
          <TextInput
            style={{
              marginTop: 5,
              height: 45,
              borderRadius: 10,
              paddingLeft: 10,
              paddingRight: 10,
              elevation: 4,
              backgroundColor: 'white',
              fontWeight: 'bold',
            }}
            placeholder="Email atau No. Handphone"
            placeholderTextColor="rgba(104,113,118,1.00)"
          />
        </View>
        <View
          style={{
            marginBottom: 10,
          }}>
          <Text
            style={{
              fontWeight: 'bold',
            }}>
            Password
          </Text>
          <View
            style={{
              elevation: 4,
              backgroundColor: 'white',
              height: 45,
              borderRadius: 10,
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <TextInput
              secureTextEntry={true}
              style={{
                marginTop: 5,
                paddingLeft: 10,
                paddingRight: 10,
                width: '90%',
                fontWeight: 'bold',
              }}
              placeholder="Password"
              placeholderTextColor="rgba(104,113,118,1.00)"
            />
            <TouchableOpacity>
              <EyesSvg width={20} height={20} fill="black" />
            </TouchableOpacity>
          </View>
        </View>
        <Button
          style={{
            width: '100%',
            justifyContent: 'center',
            borderRadius: 10,
            backgroundColor: 'rgba(255,94,31,1.00)',
          }}>
          <Text
            style={{
              fontSize: 17,
              color: 'white',
              fontWeight: 'bold',
              textTransform: 'uppercase',
            }}>
            Log in
          </Text>
        </Button>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            marginTop: 15,
            marginBottom: 15,
          }}>
          <View
            style={{
              flex: 1,
              height: 0.8,
              backgroundColor: 'rgba(205,208,209,1.00)',
            }}
          />
          <View>
            <Text
              style={{
                width: 50,
                textAlign: 'center',
                color: 'rgba(104,113,118,1.00)',
              }}>
              atau
            </Text>
          </View>
          <View
            style={{
              flex: 1,
              height: 0.8,
              backgroundColor: 'rgba(205,208,209,1.00)',
            }}
          />
        </View>
      </View>
      <Button
        style={{
          width: '100%',
          marginBottom: 10,
          borderRadius: 10,
          justifyContent: 'center',
          backgroundColor: 'rgba(59,89,152,1.00)',
        }}>
        <View
          style={{
            width: 100,
            alignItems: 'center',
            flexDirection: 'row',
          }}>
          <Facebook width={20} height={20} />
          <Text
            style={{
              marginLeft: 10,
              color: 'white',
              fontWeight: 'bold',
            }}>
            Facebook
          </Text>
        </View>
      </Button>
      <Button
        style={{
          width: '100%',
          marginBottom: 10,
          borderRadius: 10,
          justifyContent: 'center',
          backgroundColor: 'rgba(66,133,244,1.00)',
        }}>
        <View
          style={{
            width: 100,
            alignItems: 'center',
            flexDirection: 'row',
          }}>
          <Google width={20} height={20} />
          <Text
            style={{
              marginLeft: 10,
              color: 'white',
              fontWeight: 'bold',
            }}>
            Google
          </Text>
        </View>
      </Button>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <TouchableOpacity onPress={onRouter.bind('', 'forgot')}>
          <Text
            style={{
              fontWeight: 'bold',
              color: 'rgba(1,148,243,1.00)',
            }}>
            Lupa Password?
          </Text>
        </TouchableOpacity>
        <View
          style={{
            width: 1,
            height: 20,
            backgroundColor: 'black',
            marginRight: 10,
            marginLeft: 10,
          }}
        />
        <TouchableOpacity onPress={onRouter.bind('', 'register')}>
          <Text
            style={{
              fontWeight: 'bold',
              color: 'rgba(1,148,243,1.00)',
            }}>
            Belum memiliki akun?
          </Text>
        </TouchableOpacity>
      </View>
    </Animated.View>
  );
};

export default LoginComponent;
