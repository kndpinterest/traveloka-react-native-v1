import React from 'react';
import { NavigationProp } from '../../../../routes/composite';
import ForgotComponent from './component/forgot.component';
import LoginComponent from './component/login.component';
import RegisterComponent from './component/register.component';

export type CredentialsScreenState = 'login' | 'register' | 'forgot';

export interface ContextProps {
  screen: CredentialsScreenState;
  navigation: NavigationProp
}

export const CredentialsContext = React.createContext<ContextProps>({});
export const CredentialsContextApp = () => {
  return (
    <CredentialsContext.Consumer>
      {({screen, navigation}) => {
        if (screen === 'login') {
          return <LoginComponent navigation={navigation} />;
        } else if (screen === 'register') {
          return <RegisterComponent navigation={navigation} />;
        } else if (screen === 'forgot') {
          return <ForgotComponent navigation={navigation} />;
        }
      }}
    </CredentialsContext.Consumer>
  );
};
