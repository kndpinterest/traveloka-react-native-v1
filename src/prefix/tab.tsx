import React from 'react'
import AwalSvg from '../assets/svg/homepage.svg';
import UserSvg from '../assets/svg/user.svg'
import ListOrderSvg from '../assets/svg/checklist.svg'
import ListOrderSaveSvg from '../assets/svg/delivery-box.svg'

export const CustomTabDefault = [{
    name: "Awal",
    icon: <AwalSvg width={20} height={20} fill="black"/>
},{
    name: "Simpan",
    icon: <ListOrderSaveSvg width={20} height={20} stroke="black" strokeWidth={25}/>
},{
    name: "Pesanan Saya",
    icon: <ListOrderSvg width={20} height={20} stroke="black" strokeWidth={25}/>
},{
    name: "Akun Saya",
    icon: <UserSvg width={20} height={20} stroke="black" strokeWidth={35}/>
}]