import React from 'react';
import AwalSvg from '../assets/svg/homepage.svg';
import UserSvg from '../assets/svg/UserFromTraveloka.svg';
import PesanSvg from '../assets/svg/Pesan.svg';
import HelpSvg from '../assets/svg/help_.svg';
import TeleponSvg from '../assets/svg/telepon.svg';
import IndoSvg from '../assets/svg/indo.svg';
import DollarSvg from '../assets/svg/Dollar.svg';
import InstallApp from '../assets/svg/install_app.svg';
import PromoSvg from '../assets/svg/promo.svg';
import PesawatSvg from '../assets/svg/Pesawat.svg';
import HotelSvg from '../assets/svg/Hotel.svg';
import KeretaAPI from '../assets/svg/KeretaAPI.svg';
import TravelSvg from '../assets/svg/Travel.svg';
import Experience from '../assets/svg/Experience.svg';
import RentalMobileSvg from '../assets/svg/rental_mobil.svg';
import InternetSvg from '../assets/svg/Internet.svg';
import InternetLuarSvg from '../assets/svg/internet_luar_negeri.svg';
import JemputSvg from '../assets/svg/Jemput_cars.svg';
import GifSvg from '../assets/svg/gif.svg';
import AsuransiSvg from '../assets/svg/asuransi.svg';
import TravelokaSvg from '../assets/svg/travelokaSvg.svg';
import KarirSvg from '../assets/svg/karir.svg';
import BlogSvg from '../assets/svg/blog.svg';
import CaraPesanSvg from '../assets/svg/cara_pesan.svg';
import PushNotificationSvg from '../assets/svg/pust_notification.svg';
import CoraporatesSvg from '../assets/svg/carporates.svg';
import AffiliteSvg from '../assets/svg/affiliace.svg';
import PrivateSvg from '../assets/svg/private.svg';
import { DrawerListProp } from '../routes/drawer';

export const MenuArray: DrawerListProp[] = [
  {
    name: 'Awal Kenedi Novriansyah',
    icon: <AwalSvg width={20} height={20} fill="black" />,
    router: 'defaultStack',
    params: 'login'
  },
  {
    name: 'Masuk',
    icon: <UserSvg width={20} height={20} fill="black" />,
    router: 'credentials',
    params: 'login'
  },
  {
    name: 'Pesanan Saya',
    icon: <PesanSvg width={20} height={20} fill="black" />,
    router: 'credentials',
    params: 'login'
  },
  {
    name: 'Pusat Bantuan',
    icon: <HelpSvg width={20} height={20} fill="black" />,
    router: 'credentials',
    params: 'login'
  },
  {
    name: 'Hubungi Kami',
    icon: <TeleponSvg width={20} height={20} fill="black" />,
    router: 'credentials',
    params: 'login'
  },
  {
    name: 'Negara & Bahasa',
    icon: <IndoSvg width={20} height={20} fill="black" />,
    router: 'credentials',
    params: 'login'
  },
  {
    name: 'Mata Uang',
    icon: <DollarSvg width={20} height={20} fill="black" />,
    router: 'credentials',
    params: 'login'
  },
  {
    name: 'Unduh Aplikasi',
    icon: <InstallApp width={20} height={20} fill="black" />,
    router: 'credentials',
    params: 'login'
  },
  {
    name: 'Promo',
    icon: <PromoSvg width={20} height={20} fill="black" />,
    router: 'credentials',
    params: 'login'
  },
  {
    name: 'Tiket Pesawat',
    icon: <PesawatSvg width={20} height={20} fill="black" />,
    router: 'credentials',
    params: 'login'
  },
  {
    name: 'Hotel',
    icon: <HotelSvg width={20} height={20} fill="black" />,
    router: 'credentials',
    params: 'login'
  },
  {
    name: 'Tiket Kereta API',
    icon: <KeretaAPI width={20} height={20} fill="black" />,
    router: 'credentials',
    params: 'login'
  },
  {
    name: 'Tiket Bus & Travel',
    icon: <TravelSvg width={20} height={20} fill="black" />,
    router: 'credentials',
    params: 'login'
  },
  {
    name: 'Xperinence',
    icon: <Experience width={20} height={20} fill="black" />,
    router: 'credentials',
    params: 'login'
  },
  {
    name: 'Rental Mobil',
    icon: <RentalMobileSvg width={20} height={20} fill="black" />,
    router: 'credentials',
    params: 'login'
  },
  {
    name: 'Pulsa & Paket Internet',
    icon: <InternetSvg width={20} height={20} fill="black" />,
    router: 'credentials',
    params: 'login'
  },
  {
    name: 'Internet luar negeri',
    icon: <InternetLuarSvg width={20} height={20} fill="black" />,
    router: 'credentials',
    params: 'login'
  },
  {
    name: 'Antar jemput bandara',
    icon: <JemputSvg width={20} height={20} fill="black" />,
    router: 'credentials',
    params: 'login'
  },
  {
    name: 'Gif voucher',
    icon: <GifSvg width={20} height={20} fill="black" />,
    router: 'credentials',
    params: 'login'
  },
  {
    name: 'Asuransi',
    icon: <AsuransiSvg width={20} height={20} fill="black" />,
    router: 'credentials',
    params: 'login'
  },
  {
    name: 'Tentang Traveloka',
    icon: <TravelokaSvg width={20} height={20} fill="black" />,
    router: 'credentials',
    params: 'login'
  },
  {
    name: 'Karir',
    icon: <KarirSvg width={20} height={20} fill="black" />,
    router: 'credentials',
    params: 'login'
  },
  {
    name: 'Blog',
    icon: <BlogSvg width={20} height={20} fill="black" />,
    router: 'credentials',
    params: 'login'
  },
  {
    name: 'Cara Pesan',
    icon: <CaraPesanSvg width={20} height={20} fill="black" />,
    router: 'credentials',
    params: 'login'
  },
  {
    name: 'Cara pesan',
    icon: <PesanSvg width={20} height={20} fill="black" />,
    router: 'credentials',
    params: 'login'
  },
  {
    name: 'Push Notification',
    icon: <PushNotificationSvg width={20} height={20} fill="black" />,
    router: 'credentials',
    params: 'login'
  },
  {
    name: 'Traveloka for corporates',
    icon: <CoraporatesSvg width={20} height={20} fill="black" />,
    router: 'credentials',
    params: 'login'
  },
  {
    name: 'Traveloka affiliate',
    icon: <AffiliteSvg width={20} height={20} fill="black" />,
    router: 'credentials',
    params: 'login'
  },
  {
    name: 'Kebijakan privasi',
    icon: <PrivateSvg width={20} height={20} fill="black" />,
    router: 'credentials',
    params: 'login'
  },
  {
    name: 'Syarat & Ketentuan',
    icon: <AwalSvg width={20} height={20} fill="black" />,
    router: 'credentials',
    params: 'login'
  },
];
